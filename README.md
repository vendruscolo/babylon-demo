# Babylon demo notes

## The app (specs)
Welcome to “JSONPlaceholder News”. This is like HackerNews, but for X (pun intended).
The application shows first a list of “news” (posts, in JSONPlaceholder parlance) allowing users to pick one and see its comments. The application is read-only, and doesn’t need users to sign in.

JSONPlaceholder News, as platform, has some restrictions: once users sign up, they won’t be able to change their profile data (that’s bad for user experience but it’s good for developers, as they can cache user profiles forever! [It’s a bit lame, I admit it]).

JSONPlaceholder News (iOS app) requirements:
* Display the full list of news;
* Allow to see a news detail;
* Display at least the name of news’ authors;
* Display the comments for each news;
* For each comment, display the author email and comment text;
* The app must work when offline, showing previously cached data.

## Inspiration
https://dribbble.com/shots/5958932-Blys-App-homescreen-interface/
The above shot has been used for inspiration in building the list of posts (although Posts don’t have any associated timestamp). I like the color palette too, so it’s been used to define the custom colors of the app (using the asset catalog).

With swiftgen a swift type is generated automatically to provide programmatic and type safe access to those colors.

## Icons
https://material.io/tools/icons/
I imported icons from Google Material Design, which are available under an Apache 2.0 License.
Icons have been downloaded as SVG, converted to PDF to be used as vector (the app supports dynamic type).
Icons are set to be used as template images, so it’s possible to have just one asset that gets colored at render time: this gives great flexibility for themes.

Swiftgen has been integrated here too to provide programmatic access to assets.

## Development

Make sure you have [`bundler`](https://bundler.io/) installed.
Otherwise, run the following commands to install them:

```bash
$ gem install bundler
```

### Get started

1. Clone the project;
2. In the project dir, run a `bundle install`;
3. Run `bundle exec pod install` for installing the project dependencies;
4. Now you're able to open the project with Xcode (the \`.xcworkspace\` file!).


## Architecture
The application has been developed keeping the “Separation of concerns” in mind. It’s in my opinion the most important design principle to follow, along with inversion of control and decoupling.
The app has many different types that focus on a single task, to keep things simple and easier to understand.

One example of this approach can be seen in the commit 1fe7401 (please don’t mind the implicitly unwrapped optional!): this one upgrades the view model to use a different type to perform the real HTTP request to fetch data. The UI (view controller and view) hasn’t been affected by this change.

Separation of concerns has many benefits:
1. Allows the team to split the work and work in parallel;
2. Minimizes the changes required when integrating things together;
3. Keeps types simpler, which leads to better testability;
4. Ultimately, tends to help the team achieve _design by contract_.

I’ve borrowed some aspects (`*UseCases`, `*Repository`) from the Clean Architecture, but it’s not implemented blindly in its entirety.

Other things to keep in mind:
1. ViewModels are in place to ease data transformation from domain to UI;
2. `Observable`s are used to glue together entities. They fit really well because they compose well and encapsulate errors nicely;
3. ReSwift has been used to have a unidirectional data flow, a-la Redux. Its state might even make view models useless.
4. ViewControllers are treated to be views. They lay out “real” views, bind data from the view model to the view performing no transformation, and react to user interactions (passing them back to the view model, which then dispatches the correct event to the store).

Another example of separation of concerns and inversion of control that I really enjoyed is e4f272c. In this case I’ve added support to local fetching from the cache first. We can see the `CommentsUseCases` receiving a dependency it needs, so it doesn’t have to bother creating one.
The method now emits two results, so it’s been changed from returning a `Single` to returning an `Observable`. The thunk, in turn, has been updated to match the new signature (`onSuccess:` vs `onNext:`) and that’s it.
It updates the state as before, the view model and the view layer “didn’t notice” any change.

In 7925094 you can see how adding a feature affects all the entities and data flow.
The application is comprised of many small pieces, that can be glued together fairly easily, thanks to Rx.
For example, the `UserUseCases` has a different logic to provide the data (fetches locally and if that fails, fetches remotely). It connects together two smaller pieces (`UsersDAO`, `UsersRepository`).
With this example we also see the importance of using RxSwift: data returned by each piece is encapsulated in an Observable with proper semantics (or, in RxSwift parlance, traits).
This allows to compose together smaller things: in this case I used `catchError` to switch to the repository if the DAO fails to provide data. In the other use cases I used `concat` to perform both fetches sequentially.

## Inversion of control / Dependency injection
I’ve followed this pattern for injecting dependencies: https://www.swiftbysundell.com/posts/dependency-injection-using-factories-in-swift
Not a fan of swinject personally. I used it at work, but didn’t convince me.
Pros of swinject: you follow their pattern, but it’s more complicated.
Pros of John’s approach: easier, but you *have to sort of invent it*.
What I like the most about John’s approach (or more broadly, about factories) is that they make the call site really nice: see this commit 543f6d4.

## Unidirectional data flow
I’ve used ReSwift to implement unidirectional data flow.
The app is pretty simple, but the unidirectional data flow makes it easy to reason about the data flow.

There’s only one place I’m not entirely happy about the outcome. ReSwift should be used to manage (or help manage) the navigation state, so it can work together with flow coordinators.
ReSwift-Router was born for this use case, but it’s not mature enough.

The app flow coordinator dispatches two navigation-related actions, but I’m not entirely satisfied by this decision.
Unfortunately, navigation is encapsulated in navigation controllers and it’s hard to pull out some information to its own entity.

## Domain models
Although JSONPlaceholder data has been modeled in a not-so-common way (posts have a reference to the author but not their comments. Comments have a reference to the post they belong to), I’ve decided to keep references for all entities.
This means that:
* `Post` has references to `User` (`authorId`) and `Comment` (`commentsId`);
* `Comment` has a refence to `Post` (`postId`).

References are created using John Sundell’s `Identifiable` type (which has been put in the vendor directory, since it’s not distributed using CocoaPods).

All models don’t have any optional field.

With the above in place, it means that in order to create a `Post` we also need to have the list of its `Comment`s.
This means that the `PostsUseCases` has to fetch both entities from the network (two network requests). This is good for use cases, it shows exactly why they exist.
The `PostsDAO` on the other side has to perform a single fetch, because the structure of CoreData entities is different.

## Networking
To perform HTTP requests I’ve settled with Moya, a library developed on top of Alamofire. It also has reactive extensions.
The main selling point for Moya is the way it forces you to structure the networking code: everything is kept separate and well structured.
It also allow to mock network requests completely.

## Repositories
Networking is performed by repositories. A single `*Repository` performs the required requests using a `MoyaProvider<JSONPlaceholderTarget>` (injected) and performs the mapping needed, while also handling network errors.
A Repository hides the networking layer.

## Remote/Offline first approach
The app follows a remote first approach, which means that remote data is used (“marked”) as the main driver. This means that for each screen the app will try to fetch remote data even if it already has the whole data set available offline.
This choice is deliberate, and it’s specific for this application use case.
We decided (even if we don’t have any real hint from the API) this approach provides the best value for the users, improving the UX. Imagine this app shows posts and comments about breaking news.
The flow of data is this:
1. Show cached data so the app feels fast;
2. Request remote data;
    1. Show a non-blocking spinner;
    2. If the request fails (offline) show a banner to inform that the data shown is stale;
3. Update the UI with the new data and cache it.

## CoreData
The `P` prefix in each class stands for “Persistence”, and serves us to prevent clashes with the domain entities.

The app uses CoreData to cache data. The approach used, as described in the previous paragraph, is to be remote first.
For this reason, we can’t store the entire object graph, because we’re not loading it at first. In particular, we’re not loading the complete user list, so we can’t fully create and store the `author` property of `Post`.
We might get around this by setting a partial user (e.g. `NullUser` or `EmptyUser`, with just the `id` set) on `PPost` or make the `authorId` property optional in `Post`. The two solutions work, but will certainly bite us in the future.

The solution chosen is to reflect the domain entities to CoreData. No relationships, store just the identifiers of the referenced objects.
See b26f942 for more information.

For the use case of the app this means that users can browse the post list offline, pick an item and view the details complete with comments. Due to the relationships between entities, this users can also view the comments of posts even if they never navigated to the post detail (because comments are loaded at start). In such case they wouldn’t be able to view the author information, and this has been considered to be acceptable.

## DAOs
Just like repositories, for each entity that can be persisted there’s a DAO that simplifies the interactions and hides the database.

## UseCases
In a typical Clean Architecture fashion, `*UseCases`’ consume DAOs and repositories data.

## FlowCoordinators
I’ve been using flow coordinators for a few years, before they went mainstream. My first implementation of “an object whose responsibility is to manage the navigation of an app” dates back to Swift 2, and I used extensively the NSNotificationCenter and was implemented by the AppDelegate (although consumers didn’t know this). I’ve been exploring this idea and it evolved in the years, getting the goods from Alberto De Bortoli, Soroush Khanlou, and many others.

The main selling point to me is this: having the navigation managed transparently by a singular entity, which encapsulates it makes it easy to change the behavior.

For example, in de1624a I embedded the first view controller in a UINavigationController and the AppDelegate didn’t require any change.
This example might sound contrived, because the app just has two scenes. But it’s really powerful.
Imagine the app has tens of scenes, with complex navigation. This allows to skip directly to a specific scene (reduces developers time as they won’t have to drill down into the view controller hierarchy when working on a specific feature) and provides better reuse of sub-flows (e.g. a flow that allows users to pick a photo from their gallery).

## Testing
I’ve included unit tests for the most important entities. It includes domain models, redux (thunks, reducers), repositories, use cases, and view models.
The parts that have not been tested are the DAOs, the flow coordinator, the view controllers and the views.

To test the DAOs I’d create an in-memory CoreData persistent container and verify that the DAOs perform the proper fetch requests and insert the right objects. Still, we can treat CoreData to be mature enough, and given the simple tasks the DAOs perform, I considered to be acceptable not to have tests in place.

Also, the `func authorProfileImage(from post: Post, using pipeline: ImagePipeline) -> Driver<UIImage>` function has not been tested. In order to test this, I’d need to change its signature to receive the placeholder image and to receive a mocked pipeline (I’d go with receiving a closure that receives an URL and returns an Observable).

Testing Observables has been possible by using the `toBlocking()` operator, and the RxTest facilities. For simple tests blocking the Observable is fine. For more complex scenarios (like view models) I had to use the TestScheduler scheduler provided by RxTest.

Testing was greatly simplified by doing inversion of control (that’s the approach I’d follow to test the author profile image fetching) and protocol-mocking. Every system under test receives the dependencies which are defined using protocol (so they are interfaces).
The test bundle provides stubbing version of DAOs, Repositories, UseCases, etc to better verify the expectations.

I’ve used Nimble to perform matches. I believe that it makes tests more readable.
I’ve not used Quick, tough. I’ve used it in the past and it didn’t quite convince me. For small test cases it might be a good alternative, but for complex scenarios the tests are far from readable. Another downside of Quick is that Xcode doesn’t recognize the tests until you actually run them.

### Testing benefits

In 18cf55b I fixed an issue that prevented the from loading posts the first time is run. After getting an idea of what the cause was, I wrote a failing test to check the issue was there and that the fix actually worked as expected.

## Nuke
Nuke is my preferred image loading library. I’ve used it in many projects and always felt solid. Other libraries were too specific, too crashy, less flexible. Nuke comes with sensible defaults, allows for extension and composition.
For example, it loads images by default using `URLSession`, but it’s easy to swap and plug in a different “Data” loader. This is needed when you have to fetch images providing authentication, and you want to reuse whatever you have in place (e.g. Moya).
The same applies to caching. It comes with a sensible default caching mechanism, still allowing developers to change it or disabling it altogether.

There’s an open branch for upgrading to Nuke 8 (which is in rc1 as of now). Nuke 8 brings a solid update to pipelines, giving a way to perform processing to images. In this way one could create an “avatar pipeline”, which is different from the main pipeline. This avatar pipeline will take care of downscaling images to a fixed size and applying rounded corners.

## Swiftlint
I've integrated swiftlint to lint the codebase. Default rules have been used, a part from the `identifier_name` rule which has been disabled (I had many `id` properties which are shorter than 3 chars).
Tests and Vendor (Pods too) folders have been excluded.

## Swiftgen
Swiftgen is used to generate Swift code to make type-safe access to all string-based CocoaTouch API.
It’s used to generate:
* Assets references (both images and colors);
* CoreData entities;
* Localization strings.

It makes API easier to use while also reducing the possibility to make errors.

## Improving the user experience
In order to improve the user experience and have better offline support, the application might proactively load extra information.
This should be considered for real-life applications and depends also from the structure of the HTTP API.

For example: the post details scene displays posts’ author data (their name). This has been implemented by requesting author’s details only when navigating to the posts’ detail screen, due to the structure of the API (different /users and /posts endpoints, usually the /posts endpoint would include also author data).
We might decide to load all authors data at start (this would make the app actually sync all data, and CoreData wouldn’t be just a cache).

## Dynamic Type and accessibility
The app supports dynamic type and every label (even avatars) are scaled accordingly.
Accessibility in general can be improved by using the app solely with VoiceOver.
At the moment I’d say the app is good but not great.

## Placeholder avatar (fun tidbit)
The placeholder has been generated online using https://avatars.dicebear.com
After a bunch of tries I set to use `Babylon` as seed. Another good one was `Babylon-Demo`, but the former looked better in the app and was happier.
https://avatars.dicebear.com/v2/bottts/Babylon.svg

## Improvements
An aspect that could be considered is to optimize or limit the amount of network requests.
We can optimize them by performing conditional GET requests (this needs to set up local HTTP caching, so NSURLSession can perform requests setting the If-None-Match and/or If-Modified-Since headers—assuming JSONPlaceholder supports them).
We can also limit the amount of requests by not performing them altogether.
We might store a timestamp of the last time data has been saved in CoreData and not perform requests if little time passed. This is more drastic and should be decided keeping in mind the use case of the application (how often is the data going to change? Is it a “Breaking news” app? Is it a “Magazine” app? Etc).

Another improvement I’d like to do is to have the “Read more…” string in another color. This means using a `NSAttributedString`, but it requires extra work to make it work with DynamicType.

I’d like to also improve the test bundle. I haven’t understood why I have to run it using a host application.
This requires launching the app, and shouldn’t be necessary for unit tests, only for UI tests.
Without that, the test bundle doesn’t compile as it references undefined symbols.
Also tests are not getting linted by swiftlint. For some aspects, this is not bad (for example, using implicitly unwrapped optionals in a test is not bad, in my opinion), but readability and consistence could benefit from swiftlint.
