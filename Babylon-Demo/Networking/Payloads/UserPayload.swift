//
//  UserPayload.swift
//  Babylon-Demo
//
//  Created by Alessandro Vendruscolo on 05/07/2019.
//  Copyright © 2019 AVEdesign. All rights reserved.
//

import Foundation

public struct UserAddressGeoPayload: Codable {
    public let lat: String
    public let lng: String
}

public struct UserAddressPayload: Codable {
    public let street: String
    public let suite: String
    public let city: String
    public let zipcode: String
    public let geo: UserAddressGeoPayload
}

public struct UserCompanyPayload: Codable {
    public let name: String
    public let catchPhrase: String
    public let bs: String
}

public struct UserPayload: Codable {
    public let id: Int
    public let name: String
    public let username: String
    public let email: String
    public let address: UserAddressPayload
    public let phone: String
    public let website: String
    public let company: UserCompanyPayload
}

public typealias UsersPayload = [UserPayload]
