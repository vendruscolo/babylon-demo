//
//  PostPayload.swift
//  Babylon-Demo
//
//  Created by Alessandro Vendruscolo on 24/06/2019.
//  Copyright © 2019 AVEdesign. All rights reserved.
//

import Foundation

public struct PostPayload: Codable {
    public let userId: Int
    public let id: Int
    public let title: String
    public let body: String
}

public typealias PostsPayload = [PostPayload]
