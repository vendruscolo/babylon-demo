//
//  CommentPayload.swift
//  Babylon-Demo
//
//  Created by Alessandro Vendruscolo on 24/06/2019.
//  Copyright © 2019 AVEdesign. All rights reserved.
//

import Foundation

public struct CommentPayload: Codable {
    public let postId: Int
    public let id: Int
    public let name: String
    public let email: String
    public let body: String
}

public typealias CommentsPayload = [CommentPayload]
