//
//  Mappers.swift
//  Babylon-Demo
//
//  Created by Alessandro Vendruscolo on 24/06/2019.
//  Copyright © 2019 AVEdesign. All rights reserved.
//

import Foundation

public enum NetworkMappers {

    public static func map(comment: CommentPayload) -> Comment {
        return Comment(
            id: Identifier<Comment>(rawValue: comment.id),
            postId: Identifier<Post>(rawValue: comment.postId),
            author: comment.email,
            title: comment.name,
            body: comment.body
        )
    }

    public static func map(post: PostPayload, comments: [CommentPayload]) -> Post {
        let matchingComments = comments
            .filter { $0.postId == post.id }
            .map { Identifier<Comment>(rawValue: $0.id) }

        return Post(
            id: Identifier<Post>(rawValue: post.id),
            title: post.title,
            body: post.body,
            authorId: Identifier<User>(rawValue: post.userId),
            commentsId: matchingComments
        )
    }

    public static func map(user: UserPayload) -> User {
        return User(
            id: Identifier<User>(rawValue: user.id),
            name: user.name,
            username: user.username
        )
    }
}
