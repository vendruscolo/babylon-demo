//
//  CommentsRepositoryType.swift
//  Babylon-Demo
//
//  Created by Alessandro Vendruscolo on 07/07/2019.
//  Copyright © 2019 AVEdesign. All rights reserved.
//

import Foundation
import RxSwift

public protocol CommentsRepositoryType {

    /// Fetches all the comments from the remote API.
    func getAllComments() -> Single<CommentsPayload>

    /// Fetches all the comments for the specified post.
    ///
    /// - Parameter postId: The id of the post to get comments of.
    func getCommentsOf(post postId: Identifier<Post>) -> Single<CommentsPayload>
}
