//
//  UsersRepositoryType.swift
//  Babylon-Demo
//
//  Created by Alessandro Vendruscolo on 07/07/2019.
//  Copyright © 2019 AVEdesign. All rights reserved.
//

import Foundation
import RxSwift

public protocol UsersRepositoryType {

    /// Fetches the information for the provided user.
    ///
    /// - Parameter id: The id of the user to get details of.
    func getUser(with id: Identifier<User>) -> Single<UserPayload>
}
