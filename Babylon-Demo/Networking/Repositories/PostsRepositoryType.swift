//
//  PostsRepositoryType.swift
//  Babylon-Demo
//
//  Created by Alessandro Vendruscolo on 07/07/2019.
//  Copyright © 2019 AVEdesign. All rights reserved.
//

import Foundation
import RxSwift

public protocol PostsRepositoryType {

    /// Fetches all the posts from the remote API.
    func getAllPosts() -> Single<PostsPayload>
}
