//
//  PostsRepository.swift
//  Babylon-Demo
//
//  Created by Alessandro Vendruscolo on 24/06/2019.
//  Copyright © 2019 AVEdesign. All rights reserved.
//

import Foundation
import Moya
import RxSwift

public struct PostsRepository: PostsRepositoryType {

    // MARK: Init

    public init(provider: MoyaProvider<JSONPlaceholderTarget>) {
        self.provider = provider
    }

    // MARK: PostsRepositoryType

    public func getAllPosts() -> Single<PostsPayload> {
        return provider.rx.request(.posts)
            .map(PostsPayload.self)
    }

    // MARK: Private properties

    private let provider: MoyaProvider<JSONPlaceholderTarget>
}
