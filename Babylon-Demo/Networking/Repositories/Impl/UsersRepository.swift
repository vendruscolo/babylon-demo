//
//  UsersRepository.swift
//  Babylon-Demo
//
//  Created by Alessandro Vendruscolo on 05/07/2019.
//  Copyright © 2019 AVEdesign. All rights reserved.
//

import Foundation
import Moya
import RxSwift

public struct UsersRepository: UsersRepositoryType {

    // MARK: Init

    public init(provider: MoyaProvider<JSONPlaceholderTarget>) {
        self.provider = provider
    }

    // MARK: UsersRepositoryType

    public func getUser(with id: Identifier<User>) -> Single<UserPayload> {
        return provider.rx.request(.user(id))
            .map(UserPayload.self)
    }

    // MARK: Private properties

    private let provider: MoyaProvider<JSONPlaceholderTarget>
}
