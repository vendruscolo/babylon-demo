//
//  CommentsRepository.swift
//  Babylon-Demo
//
//  Created by Alessandro Vendruscolo on 24/06/2019.
//  Copyright © 2019 AVEdesign. All rights reserved.
//

import Foundation
import Moya
import RxSwift

public struct CommentsRepository: CommentsRepositoryType {

    // MARK: Init

    public init(provider: MoyaProvider<JSONPlaceholderTarget>) {
        self.provider = provider
    }

    // MARK: CommentsRepositoryType

    public func getAllComments() -> Single<CommentsPayload> {
        return provider.rx.request(.comments)
            .map(CommentsPayload.self)
    }

    public func getCommentsOf(post postId: Identifier<Post>) -> Single<CommentsPayload> {
        return provider.rx.request(.commentsOf(postId))
            .map(CommentsPayload.self)
    }

    // MARK: Private properties

    private let provider: MoyaProvider<JSONPlaceholderTarget>
}
