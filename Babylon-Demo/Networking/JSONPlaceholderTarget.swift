//
//  JSONPlaceholderTarget.swift
//  Babylon-Demo
//
//  Created by Alessandro Vendruscolo on 23/06/2019.
//  Copyright © 2019 AVEdesign. All rights reserved.
//

import Foundation
import Moya

public enum JSONPlaceholderTarget {
    case posts
    case comments
    case commentsOf(Identifier<Post>)
    case user(Identifier<User>)
}

extension JSONPlaceholderTarget: TargetType {

    public var baseURL: URL {
        return URL(string: "https://jsonplaceholder.typicode.com")!
    }

    public var path: String {
        switch self {
        case .posts:
            return "/posts"
        case .comments:
            return "/comments"
        case .commentsOf:
            // Note that there's also the /posts/:id/comments endpoint, but it's
            // apparently broken as it returns the whole comments data set
            // (without filters). Use the querystring-based endpoint instead.
            return "/comments"
        case .user(let id):
            return "/users/\(id.rawValue)"
        }
    }

    public var method: Moya.Method {
        switch self {
        case .posts:
            return .get
        case .comments:
            return .get
        case .commentsOf:
            return .get
        case .user:
            return .get
        }
    }

    public var sampleData: Data {

        let resourceName: String
        switch self {
        case .posts:
            resourceName = "posts"
        case .comments:
            resourceName = "comments"
        case .commentsOf:
            resourceName = "comments"
        case .user:
            resourceName = "user"
        }

        return Bundle.main.url(forResource: resourceName, withExtension: "json")
            .flatMap { try? Data(contentsOf: $0) }
            ?? Data()
    }

    public var task: Task {
        switch self {
        case .posts:
            return .requestPlain
        case .comments:
            return .requestPlain
        case .commentsOf(let post):
            return .requestParameters(
                parameters: [
                    "postId": post.rawValue
                ],
                encoding: URLEncoding.queryString
            )
        case .user:
            return .requestPlain
        }
    }

    public var headers: [String: String]? {
        return nil
    }

}

extension JSONPlaceholderTarget: Equatable {}

public func == (lhs: JSONPlaceholderTarget, rhs: JSONPlaceholderTarget) -> Bool {

    switch (lhs, rhs) {
    case (.posts, .posts):
        return true
    case (.comments, .comments):
        return true
    case (.commentsOf(let lhsId), .commentsOf(let rhsId)):
        return lhsId == rhsId
    case (.user(let lhsId), .user(let rhsId)):
        return lhsId == rhsId
    default:
        return false
    }
}
