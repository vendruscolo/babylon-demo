//
//  TitleBodyTableViewCell.swift
//  Babylon-Demo
//
//  Created by Alessandro Vendruscolo on 04/07/2019.
//  Copyright © 2019 AVEdesign. All rights reserved.
//

import RxSwift
import UIKit

class HeadlineBodyTableViewCell: UITableViewCell {

    // MARK: Init

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        buildHierarchy()
        makeConstraints()
        styleCell()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: Public properties

    var reuseBasedDisposeBag = DisposeBag()

    lazy var introLabel: UILabel = {
        let l = UILabel()
        l.adjustsFontForContentSizeCategory = true
        l.font = .preferredFont(forTextStyle: .subheadline)
        l.textColor = Asset.Colors.textBody.color

        return l
    }()

    lazy var titleLabel: UILabel = {
        let l = UILabel()
        l.adjustsFontForContentSizeCategory = true
        l.font = .preferredFont(forTextStyle: .title2)
        l.numberOfLines = 0
        l.textColor = Asset.Colors.textTitle.color

        return l
    }()

    lazy var bodyLabel: UILabel = {
        let l = UILabel()
        l.adjustsFontForContentSizeCategory = true
        l.font = .preferredFont(forTextStyle: .body)
        l.numberOfLines = 0
        l.textColor = Asset.Colors.textBody.color

        return l
    }()

    // MARK: UITableViewCell overrides

    override func prepareForReuse() {
        reuseBasedDisposeBag = DisposeBag()

        introLabel.text = nil
        titleLabel.text = nil
        bodyLabel.text = nil
    }

    // MARK: Private methods

    private func buildHierarchy() {
        contentView.addSubview(introLabel)
        contentView.addSubview(titleLabel)
        contentView.addSubview(bodyLabel)
    }

    private func makeConstraints() {

        introLabel.topToSuperview(contentView.layoutMarginsGuide.topAnchor)
        introLabel.leadingToSuperview(contentView.layoutMarginsGuide.leadingAnchor)
        introLabel.trailingToSuperview(contentView.layoutMarginsGuide.trailingAnchor)

        titleLabel.leading(to: introLabel)
        titleLabel.topToBottom(of: introLabel)
        titleLabel.trailing(to: introLabel)

        bodyLabel.leading(to: titleLabel)
        bodyLabel.topToBottom(of: titleLabel, offset: 8)
        bodyLabel.trailing(to: titleLabel)
        bodyLabel.bottomToSuperview(contentView.layoutMarginsGuide.bottomAnchor, relation: .equalOrLess)

    }

    private func styleCell() {
        contentView.backgroundColor = Asset.Colors.background.color
    }

    // MARK: Private properties

    private var thumbnailImageWidthConstraint: NSLayoutConstraint?
}

class HeroBodyTableViewCell: HeadlineBodyTableViewCell {

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)

        introLabel.textColor = Asset.Colors.accent.color

        titleLabel.font = .preferredFont(forTextStyle: .largeTitle)
        titleLabel.textColor = Asset.Colors.brand.color
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: UIView overrides

    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        if let italicDescriptor = UIFont.preferredFont(forTextStyle: .body)
            .fontDescriptor
            .withSymbolicTraits(UIFontDescriptor.SymbolicTraits.traitItalic) {
            bodyLabel.font = UIFont(descriptor: italicDescriptor, size: 0)
        }
    }

}
