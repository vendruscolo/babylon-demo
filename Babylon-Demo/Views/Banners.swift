//
//  LoadingIndicator.swift
//  Babylon-Demo
//
//  Created by Alessandro Vendruscolo on 02/07/2019.
//  Copyright © 2019 AVEdesign. All rights reserved.
//

import TinyConstraints
import UIKit

private let alphaMultiplier: CGFloat = 0.7

/// The Banner is the base class, it's intended to be subclassed to be
/// customized to satisfy different use cases. By default, it defines common
/// UI elements (a stackview, a label) that will be used in all subclasses. It
/// also sets their appearance.
///
/// Note: the label's leading constraint isn't set.
class Banner: UIView {

    fileprivate override init(frame: CGRect) {
        super.init(frame: frame)

        tintColor = Asset.Colors.textTitle.color.withAlphaComponent(alphaMultiplier)

        layer.shadowColor = Asset.Colors.shadow.color.cgColor
        layer.shadowOffset = CGSize(width: 0, height: 4)
        layer.shadowRadius = 8
        layer.shadowOpacity = 1

        makeHierarchy()
        makeConstraints()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: Public properties

    lazy var label: UILabel = {
        let l = UILabel()
        l.lineBreakMode = .byWordWrapping
        l.numberOfLines = 0
        l.textColor = self.tintColor

        return l
    }()

    // MARK: UIView overrides

    override func tintColorDidChange() {
        super.tintColorDidChange()
        label.textColor = tintColor
    }

    // MARK: Private methods

    private func makeHierarchy() {
        blurView.contentView.addSubview(label)
        addSubview(blurView)
    }

    private func makeConstraints() {
        blurView.edgesToSuperview()

        label.topToSuperview(blurView.contentView.layoutMarginsGuide.topAnchor)
        label.trailingToSuperview(blurView.contentView.layoutMarginsGuide.trailingAnchor)
        label.bottomToSuperview(blurView.contentView.layoutMarginsGuide.bottomAnchor)

        label.setCompressionResistance(.defaultLow, for: .horizontal)
        label.setCompressionResistance(.required, for: .vertical)
        label.setContentHuggingPriority(.defaultLow, for: .horizontal)
        label.setContentHuggingPriority(.required, for: .vertical)
    }

    // MARK: Private properties

    // Make the visual effects view readable:
    // https://www.omnigroup.com/developer/how-to-make-text-in-a-uivisualeffectview-readable-on-any-background
    private lazy var blurEffect = UIBlurEffect(style: .light)
    fileprivate lazy var blurView: UIVisualEffectView = {
        let v = UIVisualEffectView(effect: self.blurEffect)
        v.backgroundColor = Asset.Colors.background.color.withAlphaComponent(alphaMultiplier)
        v.clipsToBounds = true
        v.layer.borderColor = Asset.Colors.border.color.cgColor
        v.layer.borderWidth = 1
        v.layer.cornerRadius = 8
        v.layoutMargins.left *= 2
        v.layoutMargins.right *= 2

        return v
    }()

}

/// The LoadingBanner is a simple view to show users that something is
/// happening. The application is performing some task and the user has to wait.
///
/// The view shows a spinner on the left side and a generic "Loading..." message
/// on the right.
/// Setting the `tintColor` will update both the label's text color and the
/// spinner color.
class LoadingBanner: Banner {

    // MARK: Init

    override init(frame: CGRect) {
        super.init(frame: frame)
        blurView.contentView.insertSubview(loader, at: 0)
        label.text = L10n.Generic.loading
        makeConstraints()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: UIView overrides

    override func tintColorDidChange() {
        super.tintColorDidChange()
        loader.color = tintColor
    }

    // MARK: Private methods

    private func makeConstraints() {
        loader.leadingToSuperview(blurView.contentView.layoutMarginsGuide.leadingAnchor)
        loader.trailingToLeading(of: label, offset: -8)
        loader.topToSuperview(blurView.contentView.layoutMarginsGuide.topAnchor, priority: .defaultHigh)
        loader.bottomToSuperview(blurView.contentView.layoutMarginsGuide.bottomAnchor, priority: .defaultHigh)
        loader.centerYToSuperview()

        loader.setCompressionResistance(.required, for: .horizontal)
        loader.setCompressionResistance(.required, for: .vertical)
        loader.setContentHuggingPriority(.required, for: .horizontal)
        loader.setContentHuggingPriority(.required, for: .vertical)
    }

    // MARK: Private properties

    private lazy var loader: UIActivityIndicatorView = {
        let v = UIActivityIndicatorView(style: .gray)
        v.color = self.label.textColor
        v.hidesWhenStopped = false
        v.startAnimating()

        return v
    }()
}

/// The WarningBanner is a simple view to inform users that something happened
/// and they should care about it.
///
/// The view shows an icon on the left side and a generic "Warning" message on
/// the right.
/// Setting the `tintColor` will update both the label's text color and the
/// icon color.
class WarningBanner: Banner {

    // MARK: Init

    override init(frame: CGRect) {
        super.init(frame: frame)
        blurView.contentView.insertSubview(icon, at: 0)
        label.text = L10n.Generic.warning
        makeConstraints()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: Private methods

    private func makeConstraints() {
        icon.leadingToSuperview(blurView.contentView.layoutMarginsGuide.leadingAnchor)
        icon.trailingToLeading(of: label, offset: -8)
        icon.topToSuperview(blurView.contentView.layoutMarginsGuide.topAnchor, priority: .defaultHigh)
        icon.bottomToSuperview(blurView.contentView.layoutMarginsGuide.bottomAnchor, priority: .defaultHigh)
        icon.centerYToSuperview()

        icon.setCompressionResistance(.required, for: .horizontal)
        icon.setCompressionResistance(.required, for: .vertical)
        icon.setContentHuggingPriority(.required, for: .horizontal)
        icon.setContentHuggingPriority(.required, for: .vertical)
    }

    // MARK: Private properties

    private lazy var icon: UIImageView = {
        let i = UIImageView(image: Asset.Icons.warning.image)

        return i
    }()
}
