//
//  AppearanceHelper.swift
//  Babylon-Demo
//
//  Created by Alessandro Vendruscolo on 06/07/2019.
//  Copyright © 2019 AVEdesign. All rights reserved.
//

import UIKit

struct AppearanceHelper {

    static func configure() {
        configureNavigationBar()
    }

    private static func configureNavigationBar() {
        UINavigationBar.appearance().tintColor = Asset.Colors.textTitle.color
    }

    static func configureNavigationBar(_ navBar: UINavigationBar) {
        navBar.barTintColor = Asset.Colors.background.color
        navBar.titleTextAttributes = [
            .foregroundColor: Asset.Colors.brand.color
        ]
    }
}
