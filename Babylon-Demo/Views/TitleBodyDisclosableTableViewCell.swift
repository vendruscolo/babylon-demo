//
//  TitleBodyDisclosableTableViewCell.swift
//  Babylon-Demo
//
//  Created by Alessandro Vendruscolo on 22/06/2019.
//  Copyright © 2019 AVEdesign. All rights reserved.
//

import RxSwift
import TinyConstraints
import UIKit

final class TitleBodyDisclosableTableViewCell: UITableViewCell {

    // MARK: Init

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        buildHierarchy()
        makeConstraints()
        styleCell()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: Public properties

    var reuseBasedDisposeBag = DisposeBag()

    lazy var thumbnailImageView: UIImageView = {
        let i = UIImageView()

        return i
    }()

    lazy var titleLabel: UILabel = {
        let l = UILabel()
        l.adjustsFontForContentSizeCategory = true
        l.font = .preferredFont(forTextStyle: .title2)
        l.textColor = Asset.Colors.textTitle.color

        return l
    }()

    lazy var bodyLabel: UILabel = {
        let l = UILabel()
        l.adjustsFontForContentSizeCategory = true
        l.font = .preferredFont(forTextStyle: .body)
        l.numberOfLines = 0
        l.textColor = Asset.Colors.textBody.color

        return l
    }()

    lazy var disclosureIcon: UIImageView = {
        let i = UIImageView()
        i.image = Asset.Icons.chevronRight.image
        i.tintColor = Asset.Colors.accent.color

        return i
    }()

    // MARK: UITableViewCell overrides

    override func prepareForReuse() {
        reuseBasedDisposeBag = DisposeBag()

        thumbnailImageView.image = nil
        titleLabel.text = nil
        bodyLabel.text = nil
    }

    // MARK: UIView overrides

    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        let adjustedWidth = UIFontMetrics(forTextStyle: .body).scaledValue(for: 50.0)
        thumbnailImageWidthConstraint?.constant = adjustedWidth
    }

    // MARK: Private methods

    private func buildHierarchy() {
        contentView.addSubview(thumbnailImageView)
        contentView.addSubview(titleLabel)
        contentView.addSubview(bodyLabel)
        contentView.addSubview(disclosureIcon)
    }

    private func makeConstraints() {

        thumbnailImageView.leadingToSuperview(contentView.layoutMarginsGuide.leadingAnchor)
        thumbnailImageView.topToSuperview(contentView.layoutMarginsGuide.topAnchor)
        thumbnailImageView.bottomToSuperview(contentView.layoutMarginsGuide.bottomAnchor, relation: .equalOrLess)
        thumbnailImageView.widthToHeight(of: thumbnailImageView)
        thumbnailImageWidthConstraint = thumbnailImageView.width(0)

        titleLabel.leadingToTrailing(of: thumbnailImageView, offset: 8)
        titleLabel.top(to: thumbnailImageView)
        titleLabel.trailingToLeading(of: disclosureIcon, offset: -8)

        bodyLabel.leading(to: titleLabel)
        bodyLabel.topToBottom(of: titleLabel, offset: 4)
        bodyLabel.trailing(to: titleLabel)
        bodyLabel.bottomToSuperview(contentView.layoutMarginsGuide.bottomAnchor, relation: .equalOrLess)

        disclosureIcon.trailingToSuperview(contentView.layoutMarginsGuide.trailingAnchor)
        disclosureIcon.centerYToSuperview()
        disclosureIcon.setContentHuggingPriority(.required, for: .horizontal)
        disclosureIcon.setContentHuggingPriority(.required, for: .vertical)
        disclosureIcon.setCompressionResistance(.required, for: .horizontal)
        disclosureIcon.setCompressionResistance(.required, for: .vertical)
    }

    private func styleCell() {
        contentView.backgroundColor = Asset.Colors.background.color
    }

    // MARK: Private properties

    private var thumbnailImageWidthConstraint: NSLayoutConstraint?
}
