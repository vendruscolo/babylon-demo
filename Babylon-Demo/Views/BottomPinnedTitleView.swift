//
//  BottomPinnedTitleView.swift
//  Babylon-Demo
//
//  Created by Alessandro Vendruscolo on 04/07/2019.
//  Copyright © 2019 AVEdesign. All rights reserved.
//

import TinyConstraints
import UIKit

final class BottomPinnedTitleView: UIView {

    override init(frame: CGRect) {
        super.init(frame: frame)

        addSubview(titleLabel)
        makeConstraints()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: Public UI properties

    lazy var titleLabel: UILabel = {
        let l = UILabel()
        l.adjustsFontForContentSizeCategory = true
        l.font = .preferredFont(forTextStyle: .headline)
        l.numberOfLines = 0
        l.textColor = Asset.Colors.textTitle.color

        return l
    }()

    // MARK: Private methods

    private func makeConstraints() {
        titleLabel.topToSuperview(layoutMarginsGuide.topAnchor, relation: .equalOrGreater)
        titleLabel.trailingToSuperview(layoutMarginsGuide.trailingAnchor)
        titleLabel.bottomToSuperview(layoutMarginsGuide.bottomAnchor)
        titleLabel.leadingToSuperview(layoutMarginsGuide.leadingAnchor)
    }
}
