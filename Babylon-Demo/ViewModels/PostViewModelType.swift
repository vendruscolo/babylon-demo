//
//  PostViewModelType.swift
//  Babylon-Demo
//
//  Created by Alessandro Vendruscolo on 26/06/2019.
//  Copyright © 2019 AVEdesign. All rights reserved.
//

import Foundation
import Nuke
import RxCocoa

protocol PostViewModelType {

    /// The post this view model refers to.
    var post: Post { get }

    /// The profile picture of the author.
    var authorProfilePicture: Driver<UIImage> { get }

    /// The comments that have been added to the post.
    var commentsList: Driver<[Comment]> { get }

    /// A string, localized, that forms the phrase "Written by %@", suitable for
    /// intros.
    var writtenBy: Driver<String> { get }

    /// An excerpt of the post. Use this if you want to provide a short preview
    /// of the post.
    ///
    /// - Parameter readMore: A string that gets appended at the end of the
    ///   excerpt.
    func excerpt(readMore: String) -> String

}

extension PostViewModelType {

    func excerpt(readMore: String) -> String {

        let excerpt = PostViewModel.truncateBody(of: post)

        guard !readMore.isEmpty else {
            return excerpt
        }

        return excerpt + "\n" + readMore
    }
}

extension PostViewModelType {

    static func truncateBody(of post: Post) -> String {
        return post.body
            .split(separator: " ")
            .prefix(upTo: 10)
            .joined(separator: " ")
    }

    static func authorProfileImage(from post: Post, using pipeline: ImagePipeline) -> Driver<UIImage> {
        let eventualUserProfilePicture = User.profilePictureURL(for: post.authorId)
            .flatMap {
                pipeline.rx.loadImage(with: $0)
                    .map { $0.image }
                    .asDriver(onErrorDriveWith: .never())
            }

        let placeholder = Driver.just(Asset.placeholderAvatar.image)

        return Driver.concat([
            placeholder,
            eventualUserProfilePicture ?? .never()
        ])
    }
}
