//
//  PostDetailViewModel.swift
//  Babylon-Demo
//
//  Created by Alessandro Vendruscolo on 03/07/2019.
//  Copyright © 2019 AVEdesign. All rights reserved.
//

import Foundation
import Nuke
import Overture
import ReSwift
import ReSwiftRx
import RxCocoa
import RxNuke
import RxSwift
import RxSwiftExt

struct PostDetailViewModel: PostViewModelType {

    // MARK: Init

    init(post: Post, state: Observable<PostDetailState>, pipeline: ImagePipeline) {

        self.post = post

        authorProfilePicture = state
            .asDriver(onErrorJustReturn: PostDetailState(author: nil, post: post, comments: []))
            .map { $0.post }
            .flatMap(profileImageWithPipeline(pipeline))

        commentsList = state
            .map { $0.comments }
            .asDriver(onErrorJustReturn: [])

        writtenBy = state
            .map { $0.author?.name }
            .map {
                $0.flatMap(L10n.User.authorPosted)?.uppercased() ?? ""
            }
            .asDriver(onErrorJustReturn: "")
    }

    // MARK: PostViewModelType

    let post: Post
    let authorProfilePicture: Driver<UIImage>
    let commentsList: Driver<[Comment]>
    let writtenBy: Driver<String>
}

private let profileImageWithPipeline = flip(curry(PostViewModel.authorProfileImage(from:using:)))
