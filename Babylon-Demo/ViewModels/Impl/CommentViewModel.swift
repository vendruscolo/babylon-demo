//
//  CommentViewModel.swift
//  Babylon-Demo
//
//  Created by Alessandro Vendruscolo on 04/07/2019.
//  Copyright © 2019 AVEdesign. All rights reserved.
//

import Foundation

public struct CommentViewModel: CommentViewModelType {

    // MARK: Init

    public init(comment: Comment) {
        self.comment = comment
    }

    // MARK: Private properties

    private let comment: Comment

    // MARK: CommentViewModelType

    public var authorCommented: String {
        return L10n.User.authorCommented(comment.author).uppercased()
    }
}
