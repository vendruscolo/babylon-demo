//
//  PostsListViewModel.swift
//  Babylon-Demo
//
//  Created by Alessandro Vendruscolo on 22/06/2019.
//  Copyright © 2019 AVEdesign. All rights reserved.
//

import Foundation
import ReSwift
import ReSwiftRx
import RxCocoa
import RxSwift

struct PostsListViewModel: PostsListViewModelType {

    // MARK: Init

    init(state: Observable<PostsState>) {

        isLoading = state
            .map {
                if case .inProgress = $0.remoteLoadStatus {
                    return true
                }
                return false
            }
            .asDriver(onErrorJustReturn: false)

        posts = state
            .map {
                $0.posts
            }
            .asDriver(onErrorJustReturn: [])

        statusMessage = state
            .map {
                switch ($0.remoteLoadStatus, $0.postsSource) {
                case (.inProgress, .localCache) where $0.posts.isEmpty:
                    return .loading(L10n.Generic.loading)
                case (.inProgress, .localCache):
                    return .loading(L10n.Postslist.refreshing)
                case (.failed, .localCache) where $0.posts.isEmpty:
                    return .warning(L10n.Postslist.loadingFailed)
                case (.failed, .localCache):
                    return .warning(L10n.Postslist.refreshingFailed)
                default:
                    return .none
                }
            }
            .asDriver(onErrorJustReturn: .none)

    }

    // MARK: PostsListViewModelType

    let isLoading: Driver<Bool>
    let posts: Driver<[Post]>
    let statusMessage: Driver<PostsListStatusMessage>

}
