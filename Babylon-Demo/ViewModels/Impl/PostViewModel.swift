//
//  PostViewModel.swift
//  Babylon-Demo
//
//  Created by Alessandro Vendruscolo on 26/06/2019.
//  Copyright © 2019 AVEdesign. All rights reserved.
//

import Foundation
import Nuke
import RxCocoa
import RxNuke
import RxSwift

struct PostViewModel: PostViewModelType {

    // MARK: Init

    init(
        post: Post,
        commentsUseCases: CommentsUseCasesType,
        usersUseCases: UsersUseCasesType,
        pipeline: ImagePipeline
    ) {
        self.commentsUseCases = commentsUseCases
        self.userUseCases = usersUseCases

        self.post = post

        authorProfilePicture = PostViewModel.authorProfileImage(from: post, using: pipeline)

        commentsList = self.commentsUseCases
            .getCommentsOf(post: post)
            .map { $0.0 }
            .asDriver(onErrorJustReturn: [])

        writtenBy = self.userUseCases
            .getUserProfile(with: post.authorId)
            .map { L10n.User.authorPosted($0.0.username).uppercased() }
            .asDriver(onErrorJustReturn: "")
    }

    // MARK: Private properties

    private let commentsUseCases: CommentsUseCasesType
    private let userUseCases: UsersUseCasesType

    // MARK: PostViewModelType

    let post: Post
    let authorProfilePicture: Driver<UIImage>
    let commentsList: Driver<[Comment]>
    let writtenBy: Driver<String>
}
