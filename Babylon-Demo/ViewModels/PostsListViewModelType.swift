//
//  PostsListViewModelType.swift
//  Babylon-Demo
//
//  Created by Alessandro Vendruscolo on 22/06/2019.
//  Copyright © 2019 AVEdesign. All rights reserved.
//

import Foundation
import RxCocoa

enum PostsListStatusMessage {
    case none
    case loading(String)
    case warning(String)
}

protocol PostsListViewModelType {

    /// The posts to display.
    var posts: Driver<[Post]> { get }

    /// Whether the view model is busy loading remote data.
    var isLoading: Driver<Bool> { get }

    /// A message that can provide the user more information about the system
    /// status.
    var statusMessage: Driver<PostsListStatusMessage> { get }
}

extension PostsListStatusMessage: Equatable {}

func == (lhs: PostsListStatusMessage, rhs: PostsListStatusMessage) -> Bool {
    switch (lhs, rhs) {
    case (.none, .none):
        return true
    case (.loading(let lhsLoading), .loading(let rhsLoading)):
        return lhsLoading == rhsLoading
    case (.warning(let lhsWarning), .warning(let rhsWarning)):
        return lhsWarning == rhsWarning
    default:
        return false
    }
}
