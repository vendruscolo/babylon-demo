//
//  CommentViewModelType.swift
//  Babylon-Demo
//
//  Created by Alessandro Vendruscolo on 04/07/2019.
//  Copyright © 2019 AVEdesign. All rights reserved.
//

import Foundation

/// A protocol defining what data a view model for a Comment should provide.
/// This view model is intended to be used in the context of lists, where not
/// the full data of a comment is needed to be shown.
public protocol CommentViewModelType {

    /// A string, localized, that forms the phrase "<author> commented:",
    /// suitable for intros.
    var authorCommented: String { get }

}
