//
//  DependencyContainer.swift
//  Babylon-Demo
//
//  Created by Alessandro Vendruscolo on 26/06/2019.
//  Copyright © 2019 AVEdesign. All rights reserved.
//

import CoreData
import Foundation
import Moya
import Nuke
import ReSwift
import ReSwiftThunk
import RxSwift
import RxSwiftExt

class DependencyContainer {

    private lazy var postsThunkMiddleware: Middleware<AppState> = createThunksMiddleware()
    private lazy var appStore = Store<AppState>(
        reducer: appReducer,
        state: AppState(),
        middleware: [self.postsThunkMiddleware]
    )

    private lazy var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "Babylon-Demo")
        container.loadPersistentStores { _, error in
            container.viewContext.mergePolicy = NSMergeByPropertyObjectTrumpMergePolicy
            if let error = error {
                fatalError("Unable to load persistent stores: \(error)")
            }
        }
        return container
    }()

    private lazy var moyaProvider = MoyaProvider<JSONPlaceholderTarget>()

    private lazy var imagePipeline = Nuke.ImagePipeline.shared
}

extension DependencyContainer: FlowCoordinatorFactory {

    func makeAppFlowCoordinator() -> AppFlowCoordinator {
        return AppFlowCoordinator(
            store: self.appStore,
            commentsUseCases: self.makeCommentsUseCases(),
            postsUseCases: self.makePostsUseCases(),
            usersUseCases: self.makeUsersUseCases(),
            factory: self
        )
    }

}

extension DependencyContainer: UseCasesFactory {

    func makeCommentsUseCases() -> CommentsUseCases {
        return CommentsUseCases(
            commentsDAO: CommentsDAO(context: self.persistentContainer.viewContext),
            commentsRepository: CommentsRepository(provider: self.moyaProvider)
        )
    }

    func makePostsUseCases() -> PostsUseCases {
        return PostsUseCases(
            postsDAO: PostsDAO(context: self.persistentContainer.viewContext),
            postsRepository: PostsRepository(provider: self.moyaProvider),
            commentsDAO: CommentsDAO(context: self.persistentContainer.viewContext),
            commentsRepository: CommentsRepository(provider: self.moyaProvider)
        )
    }

    func makeUsersUseCases() -> UsersUseCases {
        return UsersUseCases(
            usersDAO: UsersDAO(context: self.persistentContainer.viewContext),
            usersRepository: UsersRepository(provider: self.moyaProvider)
        )
    }

}

extension DependencyContainer: ViewModelFactory {

    func makeCommentViewModel(for comment: Comment) -> CommentViewModelType {
        return CommentViewModel(comment: comment)
    }

    func makePostListViewModel() -> PostsListViewModelType {

        let state = self.appStore
            .asObservable()
            .map { $0.postsState }

        return PostsListViewModel(state: state)
    }

    func makePostDetailViewModel(for post: Post) -> PostViewModelType {

        let state = self.appStore.asObservable()
            .map { $0.postDetailState }
            .unwrap()

        return PostDetailViewModel(
            post: post,
            state: state,
            pipeline: self.imagePipeline
        )
    }

    func makePostViewModel(for post: Post) -> PostViewModelType {
        return PostViewModel(
            post: post,
            commentsUseCases: self.makeCommentsUseCases(),
            usersUseCases: self.makeUsersUseCases(),
            pipeline: self.imagePipeline
        )
    }

}
