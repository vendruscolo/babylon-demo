//
//  UseCasesFactory.swift
//  Babylon-Demo
//
//  Created by Alessandro Vendruscolo on 29/06/2019.
//  Copyright © 2019 AVEdesign. All rights reserved.
//

import Foundation

protocol UseCasesFactory {

    func makeCommentsUseCases() -> CommentsUseCases

    func makePostsUseCases() -> PostsUseCases

    func makeUsersUseCases() -> UsersUseCases

}
