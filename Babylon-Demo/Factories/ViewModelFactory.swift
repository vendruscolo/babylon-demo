//
//  ViewModelFactory.swift
//  Babylon-Demo
//
//  Created by Alessandro Vendruscolo on 26/06/2019.
//  Copyright © 2019 AVEdesign. All rights reserved.
//

import Foundation

protocol ViewModelFactory {

    func makeCommentViewModel(for comment: Comment) -> CommentViewModelType

    func makePostListViewModel() -> PostsListViewModelType

    func makePostDetailViewModel(for post: Post) -> PostViewModelType

    func makePostViewModel(for post: Post) -> PostViewModelType
}
