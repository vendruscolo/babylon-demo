//
//  FlowCoordinatorFactory.swift
//  Babylon-Demo
//
//  Created by Alessandro Vendruscolo on 02/07/2019.
//  Copyright © 2019 AVEdesign. All rights reserved.
//

import Foundation

protocol FlowCoordinatorFactory {

    func makeAppFlowCoordinator() -> AppFlowCoordinator
}
