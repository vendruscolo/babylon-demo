//
//  User.swift
//  Babylon-Demo
//
//  Created by Alessandro Vendruscolo on 21/06/2019.
//  Copyright © 2019 AVEdesign. All rights reserved.
//

import Foundation

public struct User: Identifiable {

    // MARK: Types

    public typealias RawIdentifier = Int

    // MARK: Init

    public init(id: ID, name: String, username: String) {
        self.id = id
        self.name = name
        self.username = username
    }

    // MARK: Public properties

    public let id: ID
    public let name: String
    public let username: String

    /// Generates an URL suitable to download an user avatar.
    ///
    /// - Parameter userID: The ID of the user to generate the URL for.
    /// - Returns: The URL, if any. Returns nil if the user doesn't have any
    ///   profile picture set.
    public static func profilePictureURL(for userID: Identifier<User>) -> URL? {

        // This part is likely not to exist in any real-world scenario, because
        // it's the mobile app that's determining if an user has an avatar set:
        // usually, this is driven by backend API responses.
        // PREFACE: Randomuser.me hosts a bunch of pictures to be used as user
        // photos. Specifically, they have 99 male photos, 99 female photos and
        // 9 lego photos.
        // Similar to fizzbuzz, we're going to use all of the pictures to
        // diversify the UI and also take on the case where users don't have a
        // profile picture.
        // SPEC: For ID up to 9 (included), use the female version for IDs which
        // are multiple of 2, use the male version for IDs which are multiple of
        // 3, use the lego version for IDs which are multiple of both 2 and 3.
        // Return nil in all other cases.
        // Developer's note: this is wrong on so many aspects (decision taken by
        // the app, randomuser's information leak, unlikely use case, etc).
        // It's the only part of the demo that doesn't feel production ready,
        // but at least it sports a fun twist to the app.

        let id: Int = userID.rawValue
        guard 0...9 ~= id else { return nil }

        let urlString: String
        switch (id.isMultiple(of: 2), id.isMultiple(of: 3)) {
        case (true, false):
            urlString = "https://randomuser.me/api/portraits/women/\(id).jpg"
        case (false, true):
            urlString = "https://randomuser.me/api/portraits/men/\(id).jpg"
        case (true, true):
            urlString = "https://randomuser.me/api/portraits/lego/\(id).jpg"
        case (false, false):
            return nil
        }

        return URL(string: urlString)
    }
}

public extension User {

    var profilePictureURL: URL? {
        return User.profilePictureURL(for: id)
    }

}

extension User: Equatable {}
