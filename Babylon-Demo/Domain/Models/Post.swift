//
//  Post.swift
//  Babylon-Demo
//
//  Created by Alessandro Vendruscolo on 20/06/2019.
//  Copyright © 2019 AVEdesign. All rights reserved.
//

import Foundation

public struct Post: Identifiable {

    // MARK: Types

    public typealias RawIdentifier = Int

    // MARK: Init

    public init(
        id: ID,
        title: String,
        body: String,
        authorId: Identifier<User>,
        commentsId: [Identifier<Comment>]
    ) {
        self.id = id
        self.title = title
        self.body = body
        self.authorId = authorId
        self.commentsId = commentsId
    }

    // MARK: Public properties

    public let id: ID
    public let title: String
    public let body: String
    public let authorId: Identifier<User>
    public let commentsId: [Identifier<Comment>]
}

extension Post: Equatable {}
