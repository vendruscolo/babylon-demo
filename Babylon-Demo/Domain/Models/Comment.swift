//
//  Comment.swift
//  Babylon-Demo
//
//  Created by Alessandro Vendruscolo on 21/06/2019.
//  Copyright © 2019 AVEdesign. All rights reserved.
//

import Foundation

public struct Comment: Identifiable {

    // MARK: Types

    public typealias RawIdentifier = Int

    // MARK: Init

    public init(
        id: ID,
        postId: Identifier<Post>,
        author: String,
        title: String,
        body: String
    ) {
        self.id = id
        self.postId = postId
        self.author = author
        self.title = title
        self.body = body
    }

    // MARK: Public properties

    public let id: ID
    public let postId: Identifier<Post>
    public let author: String
    public let title: String
    public let body: String
}

extension Comment: Equatable {}
