//
//  LoadStatus.swift
//  Babylon-Demo
//
//  Created by Alessandro Vendruscolo on 02/07/2019.
//  Copyright © 2019 AVEdesign. All rights reserved.
//

import Foundation

public enum LoadStatus {
    case inProgress
    case completed
    case failed(Error)
    case idle
}

extension LoadStatus: Equatable {}

public func == (lhs: LoadStatus, rhs: LoadStatus) -> Bool {
    switch (lhs, rhs) {
    case (.inProgress, .inProgress):
        return true
    case (.completed, .completed):
        return true
    case (.idle, .idle):
        return true
    default:
        return false
    }
}
