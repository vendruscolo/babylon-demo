//
//  SectionOfPostAndComment.swift
//  Babylon-Demo
//
//  Created by Alessandro Vendruscolo on 02/07/2019.
//  Copyright © 2019 AVEdesign. All rights reserved.
//

import Foundation
import RxDataSources

struct SectionOfPostAndComment {
    var header: String
    var items: [Item]
}

enum PostOrComment {
    case post(Post)
    case comment(Comment)
}

extension PostOrComment: Equatable {}
func == (lhs: PostOrComment, rhs: PostOrComment) -> Bool {
    switch (lhs, rhs) {
    case (.post(let lhsPost), .post(let rhsPost)):
        return lhsPost == rhsPost
    case (.comment(let lhsComment), .comment(let rhsComment)):
        return lhsComment == rhsComment
    default:
        return false
    }
}

extension SectionOfPostAndComment: AnimatableSectionModelType {
    typealias Item = PostOrComment
    typealias Identity = String

    init(original: SectionOfPostAndComment, items: [Item]) {
        self = original
        self.items = items
    }

    var identity: String {
        return header
    }
}

extension PostOrComment: IdentifiableType {
    typealias Identity = String

    var identity: String {
        switch self {
        case .post(let post):
            return "P\(post.id)"
        case .comment(let comment):
            return "C\(comment.id)"
        }
    }
}
