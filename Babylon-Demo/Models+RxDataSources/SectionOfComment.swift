//
//  SectionOfComment.swift
//  Babylon-Demo
//
//  Created by Alessandro Vendruscolo on 02/07/2019.
//  Copyright © 2019 AVEdesign. All rights reserved.
//

import Foundation
import RxDataSources

public struct SectionOfComment {
    public var header: String
    public var items: [Item]
}

extension SectionOfComment: AnimatableSectionModelType {
    public typealias Item = Comment
    public typealias Identity = String

    public init(original: SectionOfComment, items: [Item]) {
        self = original
        self.items = items
    }

    public var identity: String {
        return header
    }
}

extension Comment: IdentifiableType {
    public typealias Identity = ID

    public var identity: Comment.ID {
        return id
    }
}
