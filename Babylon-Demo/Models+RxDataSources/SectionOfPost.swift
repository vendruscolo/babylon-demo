//
//  SectionOfPost.swift
//  Babylon-Demo
//
//  Created by Alessandro Vendruscolo on 22/06/2019.
//  Copyright © 2019 AVEdesign. All rights reserved.
//

import Foundation
import RxDataSources

public struct SectionOfPost {
    public var header: String
    public var items: [Item]
}

extension SectionOfPost: AnimatableSectionModelType {
    public typealias Item = Post
    public typealias Identity = String

    public init(original: SectionOfPost, items: [Item]) {
        self = original
        self.items = items
    }

    public var identity: String {
        return header
    }
}

extension Post: IdentifiableType {
    public typealias Identity = ID

    public var identity: Post.ID {
        return id
    }
}
