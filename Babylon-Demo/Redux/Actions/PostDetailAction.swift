//
//  PostDetailAction.swift
//  Babylon-Demo
//
//  Created by Alessandro Vendruscolo on 02/07/2019.
//  Copyright © 2019 AVEdesign. All rights reserved.
//

import Foundation
import ReSwift

struct SetPostDetail: Action, Equatable {
    let data: Post
}

struct SetAuthorForCurrentPost: Action, Equatable {
    let data: User
}

struct SetCommentsForCurrentPost: Action, Equatable {
    let data: [Comment]
}

struct SetNoPostDetail: Action, Equatable {}
