//
//  PostsAction.swift
//  Babylon-Demo
//
//  Created by Alessandro Vendruscolo on 29/06/2019.
//  Copyright © 2019 AVEdesign. All rights reserved.
//

import Foundation
import ReSwift

struct SetPostsAreLoading: Action, Equatable {}

struct SetLocalPosts: Action, Equatable {
    let data: [Post]
}

struct SetRemotePostsCompleted: Action, Equatable {
    let data: [Post]
}

struct SetRemotePostsLoadFailed: Action {
    let error: Error
}
