//
//  PostDetailReducer.swift
//  Babylon-Demo
//
//  Created by Alessandro Vendruscolo on 02/07/2019.
//  Copyright © 2019 AVEdesign. All rights reserved.
//

import Foundation
import ReSwift

func postDetailReducer(action: Action, state: PostDetailState?) -> PostDetailState? {
    var state = state

    switch action {
    case let action as SetPostDetail:
        state = PostDetailState(author: nil, post: action.data, comments: [])
    case let action as SetAuthorForCurrentPost:
        state?.author = action.data
    case let action as SetCommentsForCurrentPost:
        state?.comments = action.data
    case is SetNoPostDetail:
        state = nil
    default:
        break
    }

    return state
}
