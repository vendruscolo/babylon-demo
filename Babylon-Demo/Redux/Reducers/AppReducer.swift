//
//  AppReducer.swift
//  Babylon-Demo
//
//  Created by Alessandro Vendruscolo on 29/06/2019.
//  Copyright © 2019 AVEdesign. All rights reserved.
//

import Foundation
import ReSwift

func appReducer(action: Action, state: AppState?) -> AppState {
    return AppState(
        postsState: postsReducer(action: action, state: state?.postsState),
        postDetailState: postDetailReducer(action: action, state: state?.postDetailState)
    )
}
