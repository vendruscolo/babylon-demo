//
//  PostsReducer.swift
//  Babylon-Demo
//
//  Created by Alessandro Vendruscolo on 29/06/2019.
//  Copyright © 2019 AVEdesign. All rights reserved.
//

import Foundation
import ReSwift

func postsReducer(action: Action, state: PostsState?) -> PostsState {
    var state = state ?? PostsState()

    switch action {
    case _ as SetPostsAreLoading:
        state.remoteLoadStatus = .inProgress
    case let action as SetLocalPosts:
        state.posts = action.data
        state.postsSource = .localCache
    case let action as SetRemotePostsCompleted:
        state.remoteLoadStatus = .completed
        state.posts = action.data
        state.postsSource = .remote
    case let action as SetRemotePostsLoadFailed:
        state.remoteLoadStatus = .failed(action.error)
    default:
        break
    }

    return state
}
