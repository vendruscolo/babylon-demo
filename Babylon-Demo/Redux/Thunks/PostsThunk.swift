//
//  PostsThunk.swift
//  Babylon-Demo
//
//  Created by Alessandro Vendruscolo on 29/06/2019.
//  Copyright © 2019 AVEdesign. All rights reserved.
//

import Foundation
import ReSwift
import ReSwiftThunk
import RxSwift

func fetchPostsThunk(useCases: PostsUseCasesType, disposeBag: DisposeBag) -> Thunk<AppState> {
    return Thunk<AppState> { dispatch, getState in
        guard let state = getState() else { return }

        if case .inProgress = state.postsState.remoteLoadStatus {
            return
        }

        dispatch(SetPostsAreLoading())

        useCases
            .getAllPosts()
            .subscribe(
                onNext: { posts, source in
                    switch source {
                    case .localCache:
                        dispatch(SetLocalPosts(data: posts))
                    case .remote:
                        dispatch(SetRemotePostsCompleted(data: posts))
                    }
                },
                onError: {
                    dispatch(SetRemotePostsLoadFailed(error: $0))
                }
            )
            .disposed(by: disposeBag)
    }
}
