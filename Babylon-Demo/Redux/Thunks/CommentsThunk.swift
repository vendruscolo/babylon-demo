//
//  CommentsThunk.swift
//  Babylon-Demo
//
//  Created by Alessandro Vendruscolo on 02/07/2019.
//  Copyright © 2019 AVEdesign. All rights reserved.
//

import Foundation
import ReSwift
import ReSwiftThunk
import RxSwift

func fetchCommentsThunk(useCases: CommentsUseCasesType, disposeBag: DisposeBag) -> Thunk<AppState> {
    return Thunk<AppState> { dispatch, getState in
        guard
            let state = getState(),
            let post = state.postDetailState?.post
        else { return }

        useCases
            .getCommentsOf(post: post)
            .subscribe(
                onNext: { comments, _ in
                    dispatch(SetCommentsForCurrentPost(data: comments))
                },
                onError: { _ in }
            )
            .disposed(by: disposeBag)
    }
}
