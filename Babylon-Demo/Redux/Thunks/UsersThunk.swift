//
//  UsersThunk.swift
//  Babylon-Demo
//
//  Created by Alessandro Vendruscolo on 06/07/2019.
//  Copyright © 2019 AVEdesign. All rights reserved.
//

import Foundation
import ReSwift
import ReSwiftThunk
import RxSwift

func fetchAuthorThunk(useCases: UsersUseCasesType, disposeBag: DisposeBag) -> Thunk<AppState> {
    return Thunk<AppState> { dispatch, getState in
        guard
            let state = getState(),
            let post = state.postDetailState?.post
        else { return }

        useCases
            .getUserProfile(with: post.authorId)
            .subscribe(
                onSuccess: { profile, _ in
                    dispatch(SetAuthorForCurrentPost(data: profile))
                },
                onError: { _ in }
            )
            .disposed(by: disposeBag)
    }
}
