//
//  PostDetailState.swift
//  Babylon-Demo
//
//  Created by Alessandro Vendruscolo on 02/07/2019.
//  Copyright © 2019 AVEdesign. All rights reserved.
//

import Foundation
import ReSwift

struct PostDetailState: StateType {
    var author: User?
    var post: Post
    var comments: [Comment]
}
