//
//  PostsState.swift
//  Babylon-Demo
//
//  Created by Alessandro Vendruscolo on 29/06/2019.
//  Copyright © 2019 AVEdesign. All rights reserved.
//

import Foundation
import ReSwift

struct PostsState: StateType {
    var remoteLoadStatus: LoadStatus = .idle
    var posts: [Post] = []
    var postsSource: DataSource = .localCache
}
