//
//  AppState.swift
//  Babylon-Demo
//
//  Created by Alessandro Vendruscolo on 29/06/2019.
//  Copyright © 2019 AVEdesign. All rights reserved.
//

import Foundation
import ReSwift

struct AppState: StateType {
    var postsState = PostsState()
    var postDetailState: PostDetailState?
}
