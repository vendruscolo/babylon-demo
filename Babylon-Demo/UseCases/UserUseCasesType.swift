//
//  UserUseCasesType.swift
//  Babylon-Demo
//
//  Created by Alessandro Vendruscolo on 07/07/2019.
//  Copyright © 2019 AVEdesign. All rights reserved.
//

import Foundation
import RxSwift

public protocol UsersUseCasesType {

    /// Gets the specified user profile.
    ///
    /// - Parameter id: The id of the user to fetch.
    func getUserProfile(with id: Identifier<User>) -> Single<(User, DataSource)>
}
