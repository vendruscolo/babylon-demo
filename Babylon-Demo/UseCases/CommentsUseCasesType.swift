//
//  CommentsUseCasesType.swift
//  Babylon-Demo
//
//  Created by Alessandro Vendruscolo on 06/07/2019.
//  Copyright © 2019 AVEdesign. All rights reserved.
//

import Foundation
import RxSwift

public protocol CommentsUseCasesType {

    /// Retrieves all comments for a given post.
    ///
    /// - Parameter post: The post to get comments of.
    func getCommentsOf(post: Post) -> Observable<([Comment], DataSource)>
}
