//
//  CommentsUseCases.swift
//  Babylon-Demo
//
//  Created by Alessandro Vendruscolo on 02/07/2019.
//  Copyright © 2019 AVEdesign. All rights reserved.
//

import Foundation
import RxSwift

public struct CommentsUseCases: CommentsUseCasesType {

    // MARK: Init

    public init(
        commentsDAO: CommentsDAOType,
        commentsRepository: CommentsRepositoryType
    ) {
        self.commentsDAO = commentsDAO
        self.commentsRepository = commentsRepository
    }

    // MARK: CommentsUseCasesType

    /// The observable will usually emit twice: the first time providing data
    /// from the local cache, the second time providing data obtained from the
    /// HTTP API. For each time the second element of the tuple will
    /// disambiguate the source of the data (first element).
    public func getCommentsOf(post: Post) -> Observable<([Comment], DataSource)> {

        let dao = commentsDAO

        let local = dao.getComments(of: post)
            .asObservable()
            .map { ($0, DataSource.localCache) }

        let remote = commentsRepository
            .getCommentsOf(post: post.id)
            .asObservable()
            .map { comments in
                comments.map { comment in
                    NetworkMappers.map(comment: comment)
                }
            }
            .flatMap { comments in
                dao.persist(comments: comments)
                    .andThen(Observable.just(comments))
            }
            .map { ($0, DataSource.remote) }

        return Observable.concat(local, remote)
    }

    // MARK: Private properties

    private let commentsDAO: CommentsDAOType
    private let commentsRepository: CommentsRepositoryType
}
