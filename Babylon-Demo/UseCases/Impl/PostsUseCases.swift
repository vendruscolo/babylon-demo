//
//  PostsUseCases.swift
//  Babylon-Demo
//
//  Created by Alessandro Vendruscolo on 24/06/2019.
//  Copyright © 2019 AVEdesign. All rights reserved.
//

import Foundation
import Overture
import RxSwift

/// The `PostsUseCases` allows to perform common operations regarding the flow
/// (fetching, caching) of Posts.
public struct PostsUseCases: PostsUseCasesType {

    // MARK: Init

    public init(
        postsDAO: PostsDAOType,
        postsRepository: PostsRepositoryType,
        commentsDAO: CommentsDAOType,
        commentsRepository: CommentsRepositoryType
    ) {
        self.postsDAO = postsDAO
        self.postsRepository = postsRepository
        self.commentsDAO = commentsDAO
        self.commentsRepository = commentsRepository
    }

    // MARK: PostsUseCasesType

    /// This use case will first fetch all posts from the HTTP API (which, by
    /// design, lacks comments information) merging comments retrieved by the
    /// same API, using a different endpoint.
    /// The observal will usually emit twice: the first time providing data
    /// from the local cache, the second time providing data obtained from the
    /// HTTP API. For each time the second element of the tuple will
    /// disambiguate the source of the data (first element).
    public func getAllPosts() -> Observable<([Post], DataSource)> {

        let postsDAO = self.postsDAO
        let commentsDAO = self.commentsDAO

        let local = postsDAO.getAllPosts()
            .asObservable()
            .map { ($0, DataSource.localCache) }

        let remote = Observable
            .combineLatest(
                postsRepository.getAllPosts().asObservable(),
                commentsRepository.getAllComments().asObservable()
            )
            .map { posts, comments in
                let postWithComments = mapperWithComments(comments)
                return (
                    posts.map(postWithComments),
                    comments.map(NetworkMappers.map(comment:))
                )
            }
            .flatMap { (posts: [Post], comments: [Comment]) -> Observable<[Post]> in
                let postsSaved = postsDAO.persist(posts: posts)
                let commentsSaved = commentsDAO.persist(comments: comments)

                return postsSaved
                    .andThen(commentsSaved)
                    .andThen(Observable.just(posts))

            }
            .map { ($0, DataSource.remote) }

        return Observable.concat(local, remote)
    }

    // MARK: Private properties

    private let postsDAO: PostsDAOType
    private let postsRepository: PostsRepositoryType
    private let commentsDAO: CommentsDAOType
    private let commentsRepository: CommentsRepositoryType
}

private let mapperWithComments = flip(curry(NetworkMappers.map(post:comments:)))
