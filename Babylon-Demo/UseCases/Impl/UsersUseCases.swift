//
//  UsersUseCases.swift
//  Babylon-Demo
//
//  Created by Alessandro Vendruscolo on 05/07/2019.
//  Copyright © 2019 AVEdesign. All rights reserved.
//

import Foundation
import RxSwift
import RxSwiftExt

public struct UsersUseCases: UsersUseCasesType {

    // MARK: Init

    public init(
        usersDAO: UsersDAOType,
        usersRepository: UsersRepositoryType
    ) {
        self.usersDAO = usersDAO
        self.usersRepository = usersRepository
    }

    // MARK: Public methods

    /// Fetches from the local cache first and then hit the remote API is no
    /// user can be found. This is possible because in JSONPlaceholder News
    /// users can't change their profile.
    public func getUserProfile(with id: Identifier<User>) -> Single<(User, DataSource)> {

        let dao = usersDAO
        let repository = usersRepository

        let local = dao.getUser(withId: id)
            .map { ($0, DataSource.localCache) }

        let remote = Single<(User, DataSource)>
            .deferred {
                return repository.getUser(with: id)
                    .map(NetworkMappers.map(user:))
                    .flatMap { user in
                        dao.persist(users: [user])
                            .andThen(.just((user, DataSource.remote)))
                    }
            }

        return local.catchError { _ in remote }

    }

    // MARK: Private properties

    private let usersDAO: UsersDAOType
    private let usersRepository: UsersRepositoryType
}
