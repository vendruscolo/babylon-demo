//
//  PostsUseCasesType.swift
//  Babylon-Demo
//
//  Created by Alessandro Vendruscolo on 07/07/2019.
//  Copyright © 2019 AVEdesign. All rights reserved.
//

import Foundation
import RxSwift

public protocol PostsUseCasesType {

    /// Retrieves all posts and their comments.
    func getAllPosts() -> Observable<([Post], DataSource)>
}
