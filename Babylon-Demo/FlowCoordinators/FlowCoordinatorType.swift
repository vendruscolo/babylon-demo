//
//  FlowCoordinatorType.swift
//  Babylon-Demo
//
//  Created by Alessandro Vendruscolo on 02/07/2019.
//  Copyright © 2019 AVEdesign. All rights reserved.
//

import UIKit

/// This protocol describes a flow coordinator. Viewed from the outside, a
/// flow coordinator only has to export its initial view controller (suppose we
/// decide to swap flow coordinators in the app delegate).
protocol FlowCoordinatorType {

    /// The initial view controller managed by the coordinator, for its scene.
    var initialViewController: UIViewController { get }

    /// Start the flow managed by the receiver.
    func start()
}
