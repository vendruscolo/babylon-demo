//
//  AppFlowCoordinator.swift
//  Babylon-Demo
//
//  Created by Alessandro Vendruscolo on 02/07/2019.
//  Copyright © 2019 AVEdesign. All rights reserved.
//

import NSObject_Rx
import ReSwift
import UIKit

class AppFlowCoordinator: NSObject {

    // MARK: Types

    typealias Factory = ViewModelFactory

    // MARK: Init

    init(
        store: Store<AppState>,
        commentsUseCases: CommentsUseCases,
        postsUseCases: PostsUseCases,
        usersUseCases: UsersUseCases,
        factory: Factory
    ) {
        self.store = store
        self.commentsUseCases = commentsUseCases
        self.postsUseCases = postsUseCases
        self.userUseCases = usersUseCases
        self.factory = factory
        super.init()
    }

    // MARK: Private flow

    private func toPostsList() {

        let postListVC = PostsListViewController(factory: factory)

        store.dispatch(
            fetchPostsThunk(
                useCases: postsUseCases,
                disposeBag: postListVC.rx.disposeBag
            )
        )

        postListVC.postSelected = toDetailOf(post:)
        postListVC.title = L10n.Postslist.title
        navigationController.pushViewController(postListVC, animated: false)
    }

    private func toDetailOf(post: Post) {

        let detailVC = PostDetailViewController(post: post, factory: factory)

        store.dispatch(SetPostDetail(data: post))
        store.dispatch(
            fetchAuthorThunk(
                useCases: userUseCases,
                disposeBag: detailVC.rx.disposeBag
            )
        )
        store.dispatch(
            fetchCommentsThunk(
                useCases: commentsUseCases,
                disposeBag: detailVC.rx.disposeBag
            )
        )

        navigationController.pushViewController(detailVC, animated: true)
    }

    // MARK: Private properties

    fileprivate let store: Store<AppState>

    private let factory: Factory

    private lazy var navigationController: UINavigationController = {
        let c = UINavigationController()
        c.delegate = self
        AppearanceHelper.configureNavigationBar(c.navigationBar)

        return c
    }()

    private let commentsUseCases: CommentsUseCases
    private let postsUseCases: PostsUseCases
    private let userUseCases: UsersUseCases

}

extension AppFlowCoordinator: FlowCoordinatorType {

    var initialViewController: UIViewController {
        return navigationController
    }

    func start() {
        toPostsList()
    }
}

extension AppFlowCoordinator: UINavigationControllerDelegate {

    func navigationController(
        _ navigationController: UINavigationController,
        didShow viewController: UIViewController,
        animated: Bool
    ) {
        guard viewController is PostsListViewController else { return }

        store.dispatch(SetNoPostDetail())
    }
}
