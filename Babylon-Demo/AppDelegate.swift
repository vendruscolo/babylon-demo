//
//  AppDelegate.swift
//  Babylon-Demo
//
//  Created by Alessandro Vendruscolo on 19/06/2019.
//  Copyright © 2019 AVEdesign. All rights reserved.
//

import UIKit
import Moya

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    private lazy var dependencies = DependencyContainer()
    private lazy var appFlowCoordinator = self.dependencies.makeAppFlowCoordinator()

    func application(
        _ application: UIApplication,
        didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?
    ) -> Bool {

        AppearanceHelper.configure()
        window = UIWindow()
        window?.rootViewController = appFlowCoordinator.initialViewController
        window?.makeKeyAndVisible()
        appFlowCoordinator.start()

        return true
    }

}
