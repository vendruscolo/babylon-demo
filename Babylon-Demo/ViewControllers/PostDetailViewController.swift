//
//  PostDetailViewController.swift
//  Babylon-Demo
//
//  Created by Alessandro Vendruscolo on 02/07/2019.
//  Copyright © 2019 AVEdesign. All rights reserved.
//

import Nuke
import RxCocoa
import RxDataSources
import RxSwift
import TinyConstraints
import UIKit

final class PostDetailViewController: UIViewController {

    typealias Factory = ViewModelFactory

    // MARK: Init

    init(post: Post, factory: Factory) {
        self.factory = factory
        self.postViewModel = factory.makePostDetailViewModel(for: post)
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: View lifecycle

    override func loadView() {
        view = UIView()

        view.addSubview(tableView)
        tableView.edgesToSuperview()
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = Asset.Colors.background.color
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        viewLifeCycleDisposeBag = DisposeBag()

        Observable
            .combineLatest(
                Observable.just(postViewModel.post),
                postViewModel.commentsList.asObservable()
            )
            .map {
                let (post, comments) = $0
                return [
                    SectionOfPostAndComment(header: "", items: [PostOrComment.post(post)]),
                    SectionOfPostAndComment(header: "Comments", items: comments.map(PostOrComment.comment))
                ]
            }
            .asDriver(onErrorJustReturn: [])
            .drive(tableView.rx.items(dataSource: dataSource))
            .disposed(by: viewLifeCycleDisposeBag)

        tableView.rx.setDelegate(self)
            .disposed(by: viewLifeCycleDisposeBag)
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        viewLifeCycleDisposeBag = DisposeBag()
    }

    // MARK: Private methods

    private func configureCell(
        dataSource: TableViewSectionedDataSource<SectionOfPostAndComment>,
        tableView: UITableView,
        indexPath: IndexPath,
        item: SectionOfPostAndComment.Item
    ) -> UITableViewCell {

        switch item {
        case .post(let post):
            return configurePostCell(tableView: tableView, indexPath: indexPath, post: post)
        case .comment(let comment):
            return configureCommentCell(tableView: tableView, indexPath: indexPath, comment: comment)
        }
    }

    private func configurePostCell(tableView: UITableView, indexPath: IndexPath, post: Post) -> HeroBodyTableViewCell {
        let dequeued = tableView.dequeueReusableCell(withIdentifier: postCellReuseId, for: indexPath)
        guard let cell = dequeued as? HeroBodyTableViewCell else {
            fatalError("Cell registration type mismatch")
        }

        cell.contentView.backgroundColor = Asset.Colors.background.color

        postViewModel.writtenBy
            .drive(cell.introLabel.rx.text)
            .disposed(by: cell.reuseBasedDisposeBag)

        cell.titleLabel.text = post.title
        cell.bodyLabel.text = post.body

        return cell
    }

    private func configureCommentCell(
        tableView: UITableView,
        indexPath: IndexPath,
        comment: Comment
    ) -> HeadlineBodyTableViewCell {
        let dequeued = tableView.dequeueReusableCell(withIdentifier: commentCellReuseId, for: indexPath)
        guard let cell = dequeued as? HeadlineBodyTableViewCell else {
            fatalError("Cell registration type mismatch")
        }

        cell.contentView.backgroundColor = Asset.Colors.background.color

        let vm = factory.makeCommentViewModel(for: comment)

        cell.contentView.layoutMargins.bottom = 24

        cell.introLabel.text = vm.authorCommented
        cell.titleLabel.text = comment.title
        cell.bodyLabel.text = comment.body

        return cell

    }

    // MARK: Private properties

    private let factory: Factory

    private let postViewModel: PostViewModelType

    private var viewLifeCycleDisposeBag = DisposeBag()

    private typealias RxDataSource = RxTableViewSectionedAnimatedDataSource<SectionOfPostAndComment>
    private lazy var dataSource: RxDataSource = {
        let d = RxDataSource(configureCell: self.configureCell)
        return d
    }()

    // MARK: Private UI properties

    private let postCellReuseId = "me.avedesign.Babylon-Demo.PostDetailViewController.PostCell"
    private let commentCellReuseId = "me.avedesign.Babylon-Demo.PostDetailViewController.CommentCell"
    private lazy var tableView: UITableView = {
        let c = UITableView(frame: .zero)
        c.register(
            HeadlineBodyTableViewCell.self,
            forCellReuseIdentifier: self.commentCellReuseId
        )
        c.register(
            HeroBodyTableViewCell.self,
            forCellReuseIdentifier: self.postCellReuseId
        )
        c.allowsSelection = false
        c.allowsMultipleSelection = false
        c.backgroundColor = Asset.Colors.background.color
        c.estimatedRowHeight = 200
        c.rowHeight = UITableView.automaticDimension

        return c
    }()
}

private let commentsSectionHeaderHeight: CGFloat = 64
extension PostDetailViewController: UITableViewDelegate {

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard section == 1 else { return nil }
        let frame = CGRect(x: 0, y: 0, width: view.bounds.width, height: commentsSectionHeaderHeight)
        let headerView = BottomPinnedTitleView(frame: frame)
        headerView.titleLabel.text = L10n.Postdetail.Comments.sectionheader
        headerView.titleLabel.textColor = Asset.Colors.accent.color
        headerView.backgroundColor = Asset.Colors.background.color
        return headerView
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 1 {
            return commentsSectionHeaderHeight
        }

        return 0
    }
}
