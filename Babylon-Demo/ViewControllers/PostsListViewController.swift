//
//  PostsListViewController.swift
//  Babylon-Demo
//
//  Created by Alessandro Vendruscolo on 20/06/2019.
//  Copyright © 2019 AVEdesign. All rights reserved.
//

import Nuke
import RxCocoa
import RxDataSources
import RxSwift
import TinyConstraints
import UIKit

final class PostsListViewController: UIViewController {

    typealias Factory = ViewModelFactory

    // MARK: Init

    init(factory: Factory) {
        self.factory = factory
        self.postListViewModel = factory.makePostListViewModel()
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: Public properties

    /// Called when the user selects a post from the list; set this closure to
    /// be informed of such action and react to it.
    var postSelected: ((Post) -> Void)?

    // MARK: View lifecycle

    override func loadView() {
        view = UIView()

        view.addSubview(tableView)
        view.addSubview(loadingBanner)
        view.addSubview(warningBanner)

        makeConstraints()
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = Asset.Colors.background.color

        tableView.rx.modelSelected(Post.self)
            .subscribe(onNext: { [weak self] item in
                self?.postSelected?(item)
            })
            .disposed(by: rx.disposeBag)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        viewLifeCycleDisposeBag = DisposeBag()

        postListViewModel.posts
            .map { [SectionOfPost(header: "", items: $0)] }
            .drive(tableView.rx.items(dataSource: dataSource))
            .disposed(by: viewLifeCycleDisposeBag)

        postListViewModel.statusMessage
            .asObservable()
            .map { [weak self] (status: PostsListStatusMessage) -> (String?, Banner?) in
                switch status {
                case .none:
                    return (nil, nil)
                case let .loading(message):
                    return (message, self?.loadingBanner)
                case let .warning(message):
                    return (message, self?.warningBanner)
                }
            }
            .subscribe(onNext: { [weak self] message, banner in
                guard let self = self else { return }

                self.loadingBanner.isHidden = true
                self.warningBanner.isHidden = true

                banner?.label.text = message
                banner?.isHidden = false

                let bannerSize = banner?.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize) ?? .zero
                self.tableView.contentInset.top = bannerSize.height
                self.tableView.scrollIndicatorInsets.top = bannerSize.height

                // Nice touch: if the tableview is partially scrolled and the
                // first item isn't yet (partially) offscreen, scroll back the
                // table view to 0,0 so we make room to display the banner.
                // The banner will then be completely visible, without
                // overlapping the first item.
                // The offset can be negative as we've just inset the content.
                if self.tableView.contentOffset.y <= 0 {
                    self.tableView.scrollRectToVisible(CGRect(x: 0, y: 0, width: 1, height: 1), animated: true)
                }
            })
            .disposed(by: viewLifeCycleDisposeBag)

    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        viewLifeCycleDisposeBag = DisposeBag()
    }

    // MARK: Private methods

    private func makeConstraints() {
        tableView.edgesToSuperview()
        makeBannerConstraints(loadingBanner)
        makeBannerConstraints(warningBanner)
    }

    private func makeBannerConstraints(_ banner: Banner) {
        banner.centerXToSuperview()
        banner.widthToSuperview(multiplier: 0.8, relation: .equalOrLess)
        banner.topToSuperview(usingSafeArea: true)
    }

    private func configureCell(
        dataSource: TableViewSectionedDataSource<SectionOfPost>,
        tableView: UITableView,
        indexPath: IndexPath,
        item: SectionOfPost.Item
    ) -> UITableViewCell {

        let dequeued = tableView.dequeueReusableCell(withIdentifier: postCellReuseId, for: indexPath)
        guard let cell = dequeued as? TitleBodyDisclosableTableViewCell else {
            fatalError("Cell registration type mismatch")
        }

        cell.contentView.backgroundColor = Asset.Colors.background.color

        let viewModel = factory.makePostViewModel(for: item)

        cell.titleLabel.text = item.title
        cell.bodyLabel.text = viewModel.excerpt(readMore: L10n.Generic.readMore)

        viewModel.authorProfilePicture
            .drive(cell.thumbnailImageView.rx.image)
            .disposed(by: cell.reuseBasedDisposeBag)

        return cell
    }

    // MARK: Private properties

    private let factory: Factory

    private let postListViewModel: PostsListViewModelType

    private var viewLifeCycleDisposeBag = DisposeBag()

    private typealias RxDataSource = RxTableViewSectionedAnimatedDataSource<SectionOfPost>
    private lazy var dataSource: RxDataSource = {
        let d = RxDataSource(configureCell: self.configureCell)
        return d
    }()

    // MARK: Private UI properties

    private lazy var loadingBanner: LoadingBanner = {
        let b = LoadingBanner()
        b.isHidden = true

        return b
    }()

    private lazy var warningBanner: WarningBanner = {
        let b = WarningBanner()
        b.isHidden = true

        return b
    }()

    private let postCellReuseId = "me.avedesign.Babylon-Demo.PostsListViewController.PostCell"
    private lazy var tableView: UITableView = {
        let c = UITableView(frame: .zero)
        c.register(
            TitleBodyDisclosableTableViewCell.self,
            forCellReuseIdentifier: self.postCellReuseId
        )
        c.allowsMultipleSelection = false
        c.backgroundColor = Asset.Colors.background.color
        c.estimatedRowHeight = 200
        c.rowHeight = UITableView.automaticDimension

        return c
    }()

}
