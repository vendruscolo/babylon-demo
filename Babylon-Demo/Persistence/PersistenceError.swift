//
//  PersistenceError.swift
//  Babylon-Demo
//
//  Created by Alessandro Vendruscolo on 05/07/2019.
//  Copyright © 2019 AVEdesign. All rights reserved.
//

import Foundation

public enum PersistenceError: Error {
    case userNotFound(Identifier<User>)
}

extension PersistenceError: Equatable {}
public func == (lhs: PersistenceError, rhs: PersistenceError) -> Bool {
    switch (lhs, rhs) {
    case (.userNotFound(let lhsId), .userNotFound(let rhsId)):
        return lhsId == rhsId
    }
}
