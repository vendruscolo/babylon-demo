//
//  NSManagedObjectContext+Babylon.swift
//  Babylon-Demo
//
//  Created by Alessandro Vendruscolo on 06/07/2019.
//  Copyright © 2019 AVEdesign. All rights reserved.
//

import CoreData
import Foundation
import RxCoreData
import RxSwift

extension Reactive where Base: NSManagedObjectContext {

    func performCompletableUpdate(updateAction: @escaping (NSManagedObjectContext) -> Void) -> Completable {
        let context = base
        return Completable.create { completable in
            do {
                try context.rx.performUpdate(updateAction: updateAction)
                completable(.completed)
            } catch {
                completable(.error(error))
            }

            return Disposables.create()
        }
    }
}
