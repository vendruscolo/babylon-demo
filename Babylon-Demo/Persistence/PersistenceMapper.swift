//
//  PersistenceMapper.swift
//  Babylon-Demo
//
//  Created by Alessandro Vendruscolo on 01/07/2019.
//  Copyright © 2019 AVEdesign. All rights reserved.
//

import CoreData
import Foundation

enum PersistenceMappers {

    static func map(post: PPost) -> Post {

        let comments = post.commentsId.map {
            Identifier<Comment>(rawValue: Int($0))
        }

        return Post(
            id: Identifier<Post>(rawValue: Int(post.id)),
            title: post.title,
            body: post.body,
            authorId: Identifier<User>(rawValue: Int(post.authorId)),
            commentsId: comments
        )
    }

    static func map(post: Post, on context: NSManagedObjectContext) -> PPost {

        let postResult = PPost(context: context)

        let matchingComments = post.commentsId.map {
            Int64($0.rawValue)
        }

        postResult.body = post.body
        postResult.id = Int64(post.id.rawValue)
        postResult.title = post.title
        postResult.authorId = Int64(post.authorId.rawValue)
        postResult.commentsId = matchingComments

        return postResult
    }

    static func map(comment: PComment) -> Comment {
        return Comment(
            id: Identifier<Comment>(rawValue: Int(comment.id)),
            postId: Identifier<Post>(rawValue: Int(comment.postId)),
            author: comment.author,
            title: comment.title,
            body: comment.body
        )
    }

    static func map(comment: Comment, on context: NSManagedObjectContext) -> PComment {
        let commentResult = PComment(context: context)
        commentResult.author = comment.author
        commentResult.body = comment.body
        commentResult.id = Int64(comment.id.rawValue)
        commentResult.postId = Int64(comment.postId.rawValue)
        commentResult.title = comment.title

        return commentResult
    }

    static func map(user: PUser) -> User {
        return User(
            id: Identifier<User>(rawValue: Int(user.id)),
            name: user.name,
            username: user.username
        )
    }

    static func map(user: User, on context: NSManagedObjectContext) -> PUser {
        let userResult = PUser(context: context)
        userResult.id = Int64(user.id.rawValue)
        userResult.name = user.name
        userResult.username = user.username

        return userResult
    }
}
