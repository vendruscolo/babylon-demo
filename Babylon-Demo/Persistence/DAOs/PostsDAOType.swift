//
//  PostsDAOType.swift
//  Babylon-Demo
//
//  Created by Alessandro Vendruscolo on 07/07/2019.
//  Copyright © 2019 AVEdesign. All rights reserved.
//

import Foundation
import RxSwift

public protocol PostsDAOType {

    /// Fetches all posts that have been persisted so far.
    func getAllPosts() -> Single<[Post]>

    /// Stores the provided posts.
    ///
    /// - Parameter posts: The posts to save locally.
    func persist(posts: [Post]) -> Completable
}
