//
//  UsersDAOType.swift
//  Babylon-Demo
//
//  Created by Alessandro Vendruscolo on 07/07/2019.
//  Copyright © 2019 AVEdesign. All rights reserved.
//

import Foundation
import RxSwift

public protocol UsersDAOType {

    /// Fetches the specified user.
    ///
    /// - Parameter id: The id of the user to fetch.
    func getUser(withId id: Identifier<User>) -> Single<User>

    /// Stores locally the provided users.
    ///
    /// - Parameter users: The users to persist.
    func persist(users: [User]) -> Completable
}
