//
//  CommentsDAOType.swift
//  Babylon-Demo
//
//  Created by Alessandro Vendruscolo on 07/07/2019.
//  Copyright © 2019 AVEdesign. All rights reserved.
//

import Foundation
import RxSwift

public protocol CommentsDAOType {

    /// Fetches all comments that have been stored locally so far.
    func getAllComments() -> Single<[Comment]>

    /// Fetches all local comments for a given post.
    ///
    /// - Parameter post: The post to retrieve comments of.
    func getComments(of post: Post) -> Single<[Comment]>

    /// Store locally the provided comments.
    ///
    /// - Parameter comments: The comments to store.
    func persist(comments: [Comment]) -> Completable
}
