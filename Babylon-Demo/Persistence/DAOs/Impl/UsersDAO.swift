//
//  UsersDAO.swift
//  Babylon-Demo
//
//  Created by Alessandro Vendruscolo on 05/07/2019.
//  Copyright © 2019 AVEdesign. All rights reserved.
//

import CoreData
import Foundation
import RxCoreData
import RxSwift
import RxSwiftExt

public struct UsersDAO: UsersDAOType {

    // MARK: Init

    public init(context: NSManagedObjectContext) {
        self.context = context
    }

    // MARK: UsersDAOType

    public func getUser(withId id: Identifier<User>) -> Single<User> {
        let request: NSFetchRequest = PUser.fetchRequest()
        request.sortDescriptors = [
            NSSortDescriptor(keyPath: \PUser.id, ascending: true)
        ]

        request.predicate = NSPredicate(format: "id == %d", id.rawValue)

        return context.rx.entities(fetchRequest: request)
            .take(1)
            .map {
                if let result = $0.first {
                    return result
                }
                throw PersistenceError.userNotFound(id)
            }
            .map(PersistenceMappers.map(user:))
            .unwrap()
            .asSingle()
    }

    public func persist(users: [User]) -> Completable {
        return context.rx.performCompletableUpdate { backgroundContext in
            users
                .compactMap {
                    PersistenceMappers.map(user: $0, on: backgroundContext)
                }
                .forEach(backgroundContext.insert(_:))
        }
    }

    // MARK: Private properties

    private let context: NSManagedObjectContext
}
