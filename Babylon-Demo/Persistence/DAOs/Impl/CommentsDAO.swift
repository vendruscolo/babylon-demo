//
//  CommentsDAO.swift
//  Babylon-Demo
//
//  Created by Alessandro Vendruscolo on 02/07/2019.
//  Copyright © 2019 AVEdesign. All rights reserved.
//

import CoreData
import Foundation
import RxCoreData
import RxSwift

public struct CommentsDAO: CommentsDAOType {

    // MARK: Init

    public init(context: NSManagedObjectContext) {
        self.context = context
    }

    // MARK: CommentsDAOType

    public func getAllComments() -> Single<[Comment]> {
        let request = _makeCommentsFetchRequest()

        return _fetchComments(request)
    }

    public func getComments(of post: Post) -> Single<[Comment]> {
        let request = _makeCommentsFetchRequest()

        let commentsId = post.commentsId.map { Int64($0.rawValue) }
        request.predicate = NSPredicate(format: "id IN %@", commentsId)

        return _fetchComments(request)
    }

    public func persist(comments: [Comment]) -> Completable {
        return context.rx.performCompletableUpdate { backgroundContext in
            comments
                .compactMap {
                    PersistenceMappers.map(comment: $0, on: backgroundContext)
                }
                .forEach(backgroundContext.insert(_:))
        }
    }

    // MARK: Private methods

    private func _makeCommentsFetchRequest() -> NSFetchRequest<PComment> {
        let request: NSFetchRequest = PComment.fetchRequest()
        request.sortDescriptors = [
            NSSortDescriptor(keyPath: \PComment.id, ascending: true)
        ]

        return request
    }

    private func _fetchComments(_ request: NSFetchRequest<PComment>) -> Single<[Comment]> {
        return context.rx.entities(fetchRequest: request)
            .map {
                $0.map(PersistenceMappers.map(comment:))
            }
            .take(1)
            .asSingle()
    }

    // MARK: Private properties
    private let context: NSManagedObjectContext
}
