//
//  PostsDAO.swift
//  Babylon-Demo
//
//  Created by Alessandro Vendruscolo on 01/07/2019.
//  Copyright © 2019 AVEdesign. All rights reserved.
//

import CoreData
import Foundation
import RxCoreData
import RxSwift

public struct PostsDAO: PostsDAOType {

    // MARK: Init

    public init(context: NSManagedObjectContext) {
        self.context = context
    }

    // MARK: PostsDAOType

    public func getAllPosts() -> Single<[Post]> {
        let request: NSFetchRequest = PPost.fetchRequest()
        request.sortDescriptors = [
            NSSortDescriptor(keyPath: \PPost.id, ascending: true)
        ]
        return context.rx.entities(fetchRequest: request)
            .map {
                $0.map(PersistenceMappers.map(post:))
            }
            .take(1)
            .asSingle()
    }

    public func persist(posts: [Post]) -> Completable {
        return context.rx.performCompletableUpdate { backgroundContext in
            posts
                .compactMap {
                    PersistenceMappers.map(post: $0, on: backgroundContext)
                }
                .forEach(backgroundContext.insert(_:))
        }
    }

    // MARK: Private properties
    private let context: NSManagedObjectContext
}
