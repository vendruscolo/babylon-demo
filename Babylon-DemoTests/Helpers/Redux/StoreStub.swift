//
//  StoreStub.swift
//  Babylon-DemoTests
//
//  Created by Alessandro Vendruscolo on 07/07/2019.
//  Copyright © 2019 AVEdesign. All rights reserved.
//

@testable import Babylon_Demo
import Foundation
import ReSwift

func noopReducer(action: Action, state: AppState?) -> AppState {
    return state ?? AppState()
}

func makeStore(initial: AppState) -> Store<AppState> {
    return Store<AppState>(reducer: noopReducer(action:state:), state: initial)
}
