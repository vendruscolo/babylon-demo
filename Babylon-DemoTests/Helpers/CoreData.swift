//
//  CoreData.swift
//  Babylon-DemoTests
//
//  Created by Alessandro Vendruscolo on 08/07/2019.
//  Copyright © 2019 AVEdesign. All rights reserved.
//

@testable import Babylon_Demo
import CoreData

func makePersistentContainer() -> NSPersistentContainer {

    let mom = NSManagedObjectModel.mergedModel(from: [Bundle.init(for: Banner.self)])!
    let container = NSPersistentContainer(name: "Babylon-DemoTests", managedObjectModel: mom)
    let description = NSPersistentStoreDescription()
    description.type = NSInMemoryStoreType
    description.shouldAddStoreAsynchronously = false

    container.persistentStoreDescriptions = [description]
    container.loadPersistentStores { (description, error) in
        if let error = error {
            fatalError("Create an in-mem coordinator failed \(error)")
        }
    }

    return container
}
