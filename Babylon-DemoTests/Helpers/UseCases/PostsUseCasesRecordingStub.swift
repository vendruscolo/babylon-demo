//
//  PostsUseCasesRecordingStub.swift
//  Babylon-DemoTests
//
//  Created by Alessandro Vendruscolo on 07/07/2019.
//  Copyright © 2019 AVEdesign. All rights reserved.
//

import Babylon_Demo
import Foundation
import RxSwift

class PostsUseCasesSuccesfulRecordingStub: PostsUseCasesType {

    init(posts: [[Post]], dataSources: [DataSource]) {
        self.posts = posts
        self.dataSources = dataSources
    }

    var requests: Int = 0
    func getAllPosts() -> Observable<([Post], DataSource)> {
        requests += 1
        return Observable.from(Array(zip(posts, dataSources)))
    }

    private let posts: [[Post]]
    private let dataSources: [DataSource]

}

class PostsUseCasesWithErrorRecordingStub: PostsUseCasesType {

    init(post: [Post], dataSource: DataSource, error: Error) {
        self.post = post
        self.dataSource = dataSource
        self.error = error
    }

    var requests: Int = 0
    func getAllPosts() -> Observable<([Post], DataSource)> {
        requests += 1

        let firstSuccess = Observable.just((post, dataSource))
        let failure = Observable<([Post], DataSource)>.error(error)
        return Observable.concat(firstSuccess, failure)
    }

    private let post: [Post]
    private let dataSource: DataSource
    private let error: Error
}
