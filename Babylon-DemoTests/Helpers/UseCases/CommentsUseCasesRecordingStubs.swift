//
//  CommentsUseCasesRecordingStubs.swift
//  Babylon-DemoTests
//
//  Created by Alessandro Vendruscolo on 06/07/2019.
//  Copyright © 2019 AVEdesign. All rights reserved.
//

import Babylon_Demo
import Foundation
import RxSwift

class CommentsUseCasesSuccesfulRecordingStub: CommentsUseCasesType {

    init(comments: [[Comment]], dataSources: [DataSource]) {
        self.comments = comments
        self.dataSources = dataSources
    }

    var requestedPosts: [Post] = []
    func getCommentsOf(post: Post) -> Observable<([Comment], DataSource)> {
        requestedPosts.append(post)
        return Observable.from(Array(zip(comments, dataSources)))
    }

    private let comments: [[Comment]]
    private let dataSources: [DataSource]
}

class PartiallyFailingCommentsUseCasesWithErrorRecordingStuff: CommentsUseCasesType {

    init(comments: [Comment], dataSource: DataSource, error: Error) {
        self.comments = comments
        self.dataSource = dataSource
        self.error = error
    }

    var requestedPosts: [Post] = []
    func getCommentsOf(post: Post) -> Observable<([Comment], DataSource)> {
        requestedPosts.append(post)

        let firstSuccess = Observable.just((comments, dataSource))
        let failure = Observable<([Comment], DataSource)>.error(error)
        return Observable.concat(firstSuccess, failure)
    }

    private let comments: [Comment]
    private let dataSource: DataSource
    private let error: Error

}

class CommentsUseCasesWithErrorRecordingStub: CommentsUseCasesType {

    init(error: Error) {
        self.error = error
    }

    var requestedPosts: [Post] = []
    func getCommentsOf(post: Post) -> Observable<([Comment], DataSource)> {
        requestedPosts.append(post)
        return Observable.error(error)
    }

    private let error: Error
}
