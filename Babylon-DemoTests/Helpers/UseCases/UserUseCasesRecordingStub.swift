//
//  UserUseCasesRecordingStub.swift
//  Babylon-DemoTests
//
//  Created by Alessandro Vendruscolo on 07/07/2019.
//  Copyright © 2019 AVEdesign. All rights reserved.
//

import Babylon_Demo
import Foundation
import RxSwift

class UsersUseCasesSuccesfulRecordingStub: UsersUseCasesType {

    init(user: User, dataSource: DataSource) {
        self.user = user
        self.dataSource = dataSource
    }

    var requestedUsers: [Identifier<User>] = []
    func getUserProfile(with id: Identifier<User>) -> Single<(User, DataSource)> {
        requestedUsers.append(id)
        return Single.just((user, dataSource))
    }

    private let user: User
    private let dataSource: DataSource

}

class UsersUseCasesWithErrorRecordingStub: UsersUseCasesType {

    init(error: Error) {
        self.error = error
    }

    var requestedUsers: [Identifier<User>] = []
    func getUserProfile(with id: Identifier<User>) -> Single<(User, DataSource)> {
        requestedUsers.append(id)
        return Single.error(error)
    }

    private let error: Error
}
