//
//  CommentsRepositoryStub.swift
//  Babylon-DemoTests
//
//  Created by Alessandro Vendruscolo on 07/07/2019.
//  Copyright © 2019 AVEdesign. All rights reserved.
//

import Babylon_Demo
import Foundation
import RxSwift

class CommentsRepositorySuccessfulStub: CommentsRepositoryType {

    init(comments: CommentsPayload) {
        self.comments = comments
    }

    var requests = 0
    func getAllComments() -> Single<CommentsPayload> {
        requests += 1
        return Single.just(comments)
    }

    var requestedComments: [Identifier<Post>] = []
    func getCommentsOf(post postId: Identifier<Post>) -> Single<CommentsPayload> {
        requestedComments.append(postId)
        return Single.just(comments)
    }


    private let comments: CommentsPayload
}

class CommentsRepositoryFailingStub: CommentsRepositoryType {

    init(error: Error) {
        self.error = error
    }

    var requests: Int = 0
    func getAllComments() -> Single<CommentsPayload> {
        requests += 1
        return Single.error(error)
    }

    var requestedComments: [Identifier<Post>] = []
    func getCommentsOf(post postId: Identifier<Post>) -> Single<CommentsPayload> {
        requestedComments.append(postId)
        return Single.error(error)
    }

    private let error: Error
}
