//
//  PostsRepositoryStub.swift
//  Babylon-DemoTests
//
//  Created by Alessandro Vendruscolo on 07/07/2019.
//  Copyright © 2019 AVEdesign. All rights reserved.
//

import Babylon_Demo
import Foundation
import RxSwift

class PostsRepositorySuccessfulStub: PostsRepositoryType {

    init(posts: PostsPayload) {
        self.posts = posts
    }

    var requests = 0
    func getAllPosts() -> Single<PostsPayload> {
        requests += 1
        return Single.just(posts)
    }

    private let posts: PostsPayload
}

class PostsRepositoryFailingStub: PostsRepositoryType {

    init(error: Error) {
        self.error = error
    }

    var requests: Int = 0
    func getAllPosts() -> Single<PostsPayload> {
        requests += 1
        return Single.error(error)
    }

    private let error: Error
}
