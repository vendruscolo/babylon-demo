//
//  UsersRepositoryStub.swift
//  Babylon-DemoTests
//
//  Created by Alessandro Vendruscolo on 07/07/2019.
//  Copyright © 2019 AVEdesign. All rights reserved.
//

import Babylon_Demo
import Foundation
import RxSwift

class UsersRepositorySuccessfulStub: UsersRepositoryType {

    init(user: UserPayload) {
        self.user = user
    }

    var requestedUsers: [Identifier<User>] = []
    func getUser(with id: Identifier<User>) -> Single<UserPayload> {
        requestedUsers.append(id)
        return Single.just(user)
    }

    private let user: UserPayload
}

class UsersRepositoryFailingStub: UsersRepositoryType {

    init(error: Error) {
        self.error = error
    }

    var requestedUsers: [Identifier<User>] = []
    func getUser(with id: Identifier<User>) -> Single<UserPayload> {
        requestedUsers.append(id)
        return Single.error(error)
    }

    private let error: Error
}
