//
//  RecordingMoyaProvider.swift
//  Babylon-DemoTests
//
//  Created by Alessandro Vendruscolo on 06/07/2019.
//  Copyright © 2019 AVEdesign. All rights reserved.
//

import Foundation
import Moya

/// A MoyaProvider that records network requests it performs and then returns
/// sample data.
class RecordingMoyaProvider<Target: TargetType>: MoyaProvider<Target> {

    /// Recorded targets that have been requested.
    var requestedTargets: [Target] = []

    static func makeRecorder() -> RecordingMoyaProvider<Target> {
        return RecordingMoyaProvider(
            endpointClosure: { target in
                return Endpoint(
                    url: target.baseURL.absoluteString + target.path,
                    sampleResponseClosure: {
                        .networkResponse(200, target.sampleData)
                    },
                    method: target.method,
                    task: target.task,
                    httpHeaderFields: nil
                )
            },
            stubClosure: { _ in .immediate }
        )
    }

    override func request(_ target: Target,
                      callbackQueue: DispatchQueue? = .none,
                      progress: ProgressBlock? = .none,
                      completion: @escaping Completion) -> Cancellable {

        requestedTargets.append(target)
        return super.request(target, callbackQueue: callbackQueue, progress: progress, completion: completion)
    }
}
