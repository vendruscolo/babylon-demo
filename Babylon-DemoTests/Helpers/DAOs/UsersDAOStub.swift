//
//  UsersDAOStub.swift
//  Babylon-DemoTests
//
//  Created by Alessandro Vendruscolo on 07/07/2019.
//  Copyright © 2019 AVEdesign. All rights reserved.
//

import Babylon_Demo
import Foundation
import RxSwift

class UsersDAOSuccessfulStub: UsersDAOType {

    init(user: User) {
        self.user = user
    }

    var requestedUsers: [Identifier<User>] = []
    func getUser(withId id: Identifier<User>) -> Single<User> {
        requestedUsers.append(id)
        return Single.just(user)
    }

    var persistedUsers: [[User]] = []
    func persist(users: [User]) -> Completable {
        persistedUsers.append(users)
        return Completable.empty()
    }

    private let user: User
}

class UsersDAOFailingStub: UsersDAOType {

    init(error: Error) {
        self.error = error
    }

    var requestedUsers: [Identifier<User>] = []
    func getUser(withId id: Identifier<User>) -> Single<User> {
        requestedUsers.append(id)
        return Single.error(error)
    }

    var persistedUsers: [[User]] = []
    func persist(users: [User]) -> Completable {
        persistedUsers.append(users)
        return Completable.error(error)
    }

    private let error: Error
}

class UsersDAOFailingGetSuccesfulStoreStub: UsersDAOType {

    init(error: Error) {
        self.error = error
    }

    var requestedUsers: [Identifier<User>] = []
    func getUser(withId id: Identifier<User>) -> Single<User> {
        requestedUsers.append(id)
        return Single.error(error)
    }

    var persistedUsers: [[User]] = []
    func persist(users: [User]) -> Completable {
        persistedUsers.append(users)
        return Completable.empty()
    }

    private let error: Error
}
