//
//  PostsDAOStub.swift
//  Babylon-DemoTests
//
//  Created by Alessandro Vendruscolo on 07/07/2019.
//  Copyright © 2019 AVEdesign. All rights reserved.
//

import Babylon_Demo
import Foundation
import RxSwift

class PostsDAOSuccessfulStub: PostsDAOType {

    init(posts: [Post]) {
        self.posts = posts
    }

    var requests = 0
    func getAllPosts() -> Single<[Post]> {
        requests += 1
        return Single.just(posts)
    }

    var persistedPosts: [[Post]] = []
    func persist(posts: [Post]) -> Completable {
        persistedPosts.append(posts)
        return Completable.empty()
    }

    private let posts: [Post]
}

class PostsDAOFailingStub: PostsDAOType {

    init(error: Error) {
        self.error = error
    }

    var requests: Int = 0
    func getAllPosts() -> Single<[Post]> {
        requests += 1
        return Single.error(error)
    }

    var persistedPosts: [[Post]] = []
    func persist(posts: [Post]) -> Completable {
        persistedPosts.append(posts)
        return Completable.error(error)
    }

    private let error: Error
}
