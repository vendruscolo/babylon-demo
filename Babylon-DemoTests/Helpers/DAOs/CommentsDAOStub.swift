//
//  CommentsDAOStub.swift
//  Babylon-DemoTests
//
//  Created by Alessandro Vendruscolo on 07/07/2019.
//  Copyright © 2019 AVEdesign. All rights reserved.
//

import Babylon_Demo
import Foundation
import RxSwift

class CommentsDAOSuccessfulStub: CommentsDAOType {

    init(comments: [Comment]) {
        self.comments = comments
    }

    var requests = 0
    func getAllComments() -> Single<[Comment]> {
        requests += 1
        return Single.just(comments)
    }

    var requestedComments: [Post] = []
    func getComments(of post: Post) -> Single<[Comment]> {
        requestedComments.append(post)
        return Single.just(comments)
    }

    var persistedComments: [[Comment]] = []
    func persist(comments: [Comment]) -> Completable {
        persistedComments.append(comments)
        return Completable.empty()
    }

    private let comments: [Comment]
}

class CommentsDAOFailingStub: CommentsDAOType {

    init(error: Error) {
        self.error = error
    }

    var requests: Int = 0
    func getAllComments() -> Single<[Comment]> {
        requests += 1
        return Single.error(error)
    }

    var requestedComments: [Post] = []
    func getComments(of post: Post) -> Single<[Comment]> {
        requestedComments.append(post)
        return Single.error(error)
    }

    var persistedComments: [[Comment]] = []
    func persist(comments: [Comment]) -> Completable {
        persistedComments.append(comments)
        return Completable.error(error)
    }

    private let error: Error
}
