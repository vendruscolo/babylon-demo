//
//  UserStubs.swift
//  Babylon-DemoTests
//
//  Created by Alessandro Vendruscolo on 06/07/2019.
//  Copyright © 2019 AVEdesign. All rights reserved.
//

import Babylon_Demo
import Foundation

func makeUserPayload(id: Int = 1) -> UserPayload {
    let json = """
{
    "id": \(id),
    "name": "Leanne Graham",
    "username": "Bret",
    "email": "Sincere@april.biz",
    "address": {
        "street": "Kulas Light",
        "suite": "Apt. 556",
        "city": "Gwenborough",
        "zipcode": "92998-3874",
        "geo": {
            "lat": "-37.3159",
            "lng": "81.1496"
        }
    },
    "phone": "1-770-736-8031 x56442",
    "website": "hildegard.org",
    "company": {
        "name": "Romaguera-Crona",
        "catchPhrase": "Multi-layered client-server neural-net",
        "bs": "harness real-time e-markets"
    }
}
""".data(using: .utf8)!

    return try! JSONDecoder().decode(UserPayload.self, from: json)
}

func makeUserWith(id: Int) -> User {
    return User(
        id: Identifier<User>(rawValue: id),
        name: "John Doe",
        username: "john.doe"
    )
}
