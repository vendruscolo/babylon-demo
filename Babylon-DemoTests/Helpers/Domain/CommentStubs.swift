//
//  CommentStubs.swift
//  Babylon-DemoTests
//
//  Created by Alessandro Vendruscolo on 06/07/2019.
//  Copyright © 2019 AVEdesign. All rights reserved.
//

import Babylon_Demo
import Foundation

func makeCommentPayload(id: Int = 23, postId: Int = 4) -> CommentPayload {
    let json = """
{
    "postId": \(postId),
    "id": \(id),
    "name": "quis tempora quidem nihil iste",
    "email": "Sophia@arianna.co.uk",
    "body": "voluptates provident repellendus iusto perspiciatis ex fugiat"
}
""".data(using: .utf8)!

    return try! JSONDecoder().decode(CommentPayload.self, from: json)
}

func makeCommentsPayload() -> CommentsPayload {
    return [
        makeCommentPayload(postId: 1),
        makeCommentPayload(postId: 2),
        makeCommentPayload(postId: 3),
        makeCommentPayload(postId: 4),
        makeCommentPayload(postId: 5)
    ]
}

func makeCommentWith(id: Int, postId: Int = 1) -> Comment {
    return Comment(
        id: Identifier<Comment>(rawValue: id),
        postId: Identifier<Post>(rawValue: postId),
        author: "The author",
        title: "A title",
        body: "And some body…"
    )
}

func makeComments() -> [Comment] {
    return [
        makeCommentWith(id: 1),
        makeCommentWith(id: 2),
        makeCommentWith(id: 3),
        makeCommentWith(id: 4),
        makeCommentWith(id: 5)
    ]
}
