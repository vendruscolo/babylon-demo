//
//  PostStubs.swift
//  Babylon-DemoTests
//
//  Created by Alessandro Vendruscolo on 06/07/2019.
//  Copyright © 2019 AVEdesign. All rights reserved.
//

import Babylon_Demo
import Foundation

func makePostPayload(id: Int = 1) -> PostPayload {
    let json = """
{
    "userId": 1,
    "id": \(id),
    "title": "sunt aut facere repellat provident occaecati excepturi optio reprehenderit",
    "body": "suscipit recusandae consequuntur expedita et cum"
}
""".data(using: .utf8)!

    return try! JSONDecoder().decode(PostPayload.self, from: json)
}

func makePostWith(id: Int) -> Post {
    return Post(
        id: Identifier<Post>(rawValue: id),
        title: "A title",
        body: "Some body",
        authorId: Identifier<User>(rawValue: 1),
        commentsId: [Identifier<Comment>(rawValue: 1)]
    )
}

func makePosts() -> [Post] {
    return [
        makePostWith(id: 1),
        makePostWith(id: 3),
        makePostWith(id: 3),
        makePostWith(id: 4),
        makePostWith(id: 5)
    ]
}
