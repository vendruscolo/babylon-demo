//
//  PostsDAOTests.swift
//  Babylon-DemoTests
//
//  Created by Alessandro Vendruscolo on 08/07/2019.
//  Copyright © 2019 AVEdesign. All rights reserved.
//

import Babylon_Demo
import Nimble
import RxBlocking
import RxSwift
import XCTest

class PostsDAOTests: XCTestCase {

    func testGetAllPostsCompletes() {
        let sut = PostsDAO(context: makePersistentContainer().viewContext)

        guard case .completed(let elements) = sut.getAllPosts().toBlocking(timeout: 1).materialize() else {
            return XCTFail()
        }

        expect(elements) == [[Post]()]
    }
}
