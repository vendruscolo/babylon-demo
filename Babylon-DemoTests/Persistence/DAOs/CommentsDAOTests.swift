//
//  CommentsDAOTests.swift
//  Babylon-DemoTests
//
//  Created by Alessandro Vendruscolo on 08/07/2019.
//  Copyright © 2019 AVEdesign. All rights reserved.
//

import Babylon_Demo
import Nimble
import RxBlocking
import RxSwift
import XCTest

class CommentsDAOTests: XCTestCase {

    let post = makePostWith(id: 1)

    func testGetAllCommentsCompletes() {
        let sut = CommentsDAO(context: makePersistentContainer().viewContext)

        guard case .completed(let elements) = sut.getAllComments().toBlocking(timeout: 1).materialize() else {
            return XCTFail()
        }

        expect(elements) == [[Comment]()]
    }

    func testGetCommentsOfCompletes() {
        let sut = CommentsDAO(context: makePersistentContainer().viewContext)

        guard case .completed(let elements) = sut.getComments(of: post).toBlocking(timeout: 1).materialize() else {
            return XCTFail()
        }

        expect(elements) == [[Comment]()]
    }
}
