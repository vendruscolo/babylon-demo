//
//  UsersDAOTests.swift
//  Babylon-DemoTests
//
//  Created by Alessandro Vendruscolo on 08/07/2019.
//  Copyright © 2019 AVEdesign. All rights reserved.
//

import Babylon_Demo
import Nimble
import RxBlocking
import RxSwift
import XCTest

class UsersDAOTests: XCTestCase {

    let userId = Identifier<User>(rawValue: 1)

    func testGetUserThrowsErrorWithEmptyContext() {

        let sut = UsersDAO(context: makePersistentContainer().viewContext)

        guard
            case .failed(let elements, let error) = sut.getUser(withId: userId).toBlocking(timeout: 1).materialize(),
            let thrownError = error as? PersistenceError
        else {
            return XCTFail()
        }

        expect(elements) == []
        expect(thrownError) == PersistenceError.userNotFound(userId)
    }
}
