//
//  UserTests.swift
//  Babylon-DemoTests
//
//  Created by Alessandro Vendruscolo on 06/07/2019.
//  Copyright © 2019 AVEdesign. All rights reserved.
//

import Babylon_Demo
import Foundation
import Nimble
import XCTest

class UserTests: XCTestCase {

    func testWomenProfilePictureURL() {
        let user = makeUserWith(id: 2)
        expect(User.profilePictureURL(for: user.id)).to(equal(URL(string: "https://randomuser.me/api/portraits/women/2.jpg")))
    }

    func testMenProfilePictureURL() {
        let user = makeUserWith(id: 3)
        expect(User.profilePictureURL(for: user.id)).to(equal(URL(string: "https://randomuser.me/api/portraits/men/3.jpg")))
    }

    func testLegoProfilePictureURL() {
        let user = makeUserWith(id: 6)
        expect(User.profilePictureURL(for: user.id)).to(equal(URL(string: "https://randomuser.me/api/portraits/lego/6.jpg")))
    }

    func testNoProfilePictureURL() {
        let negative = makeUserWith(id: -3)
        let seven = makeUserWith(id: 7)
        let eleven = makeUserWith(id: 11)

        expect(User.profilePictureURL(for: negative.id)).to(beNil())
        expect(User.profilePictureURL(for: seven.id)).to(beNil())
        expect(User.profilePictureURL(for: eleven.id)).to(beNil())
    }

}
