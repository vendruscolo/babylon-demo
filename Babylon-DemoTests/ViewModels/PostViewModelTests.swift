//
//  PostViewModelTests.swift
//  Babylon-DemoTests
//
//  Created by Alessandro Vendruscolo on 07/07/2019.
//  Copyright © 2019 AVEdesign. All rights reserved.
//

@testable import Babylon_Demo
import Nimble
import Nuke
import ReSwift
import XCTest

class PostViewModelTests: XCTestCase {

    let post = makePostWith(id: 1)
    let localComment = makeCommentWith(id: 1)
    let remoteComment = makeCommentWith(id: 2)
    let user = makeUserWith(id: 1)

    var successfulCommentsUseCases: CommentsUseCasesSuccesfulRecordingStub!
    var failingCommentsUseCases: CommentsUseCasesWithErrorRecordingStub!
    var successfulUsersUseCases: UsersUseCasesSuccesfulRecordingStub!
    var failingUsersUseCases: UsersUseCasesWithErrorRecordingStub!

    override func setUp() {
        super.setUp()
        successfulCommentsUseCases = CommentsUseCasesSuccesfulRecordingStub(comments: [[self.localComment], [self.remoteComment]], dataSources: [.localCache, .remote])
        failingCommentsUseCases = CommentsUseCasesWithErrorRecordingStub(error: TestError.network)
        successfulUsersUseCases = UsersUseCasesSuccesfulRecordingStub(user: self.user, dataSource: .localCache)
        failingUsersUseCases = UsersUseCasesWithErrorRecordingStub(error: TestError.network)
    }
    func testPost() {
        let sut = PostViewModel(post: post, commentsUseCases: successfulCommentsUseCases, usersUseCases: successfulUsersUseCases, pipeline: ImagePipeline.shared)
        expect(sut.post) == post
    }

    func testCommentsList() {

        let sut = PostViewModel(post: post, commentsUseCases: successfulCommentsUseCases, usersUseCases: successfulUsersUseCases, pipeline: ImagePipeline.shared)

        let comments = sut.commentsList.asObservable().take(1)

        guard case .completed(let elements) = comments.toBlocking().materialize() else {
            return XCTFail()
        }

        expect(self.successfulCommentsUseCases.requestedPosts) == [post]
        expect(elements.count) == 1
        expect(elements.first!) == [localComment]
    }

    func testDoesNotRelayCommentsListErrors() {

        let sut = PostViewModel(post: post, commentsUseCases: failingCommentsUseCases, usersUseCases: successfulUsersUseCases, pipeline: ImagePipeline.shared)

        let comments = sut.commentsList.asObservable().take(1)

        guard case .completed(let elements) = comments.toBlocking().materialize() else {
            return XCTFail()
        }

        expect(self.failingCommentsUseCases.requestedPosts) == [post]
        expect(elements.count) == 1
        expect(elements.first!) == []
    }

    func testWrittenBy() {

        let sut = PostViewModel(post: post, commentsUseCases: successfulCommentsUseCases, usersUseCases: successfulUsersUseCases, pipeline: ImagePipeline.shared)

        let writtenBy = sut.writtenBy.asObservable().take(1)

        guard case .completed(let elements) = writtenBy.toBlocking().materialize() else {
            return XCTFail()
        }

        expect(self.successfulUsersUseCases.requestedUsers) == [user.id]
        expect(elements.count) == 1
        expect(elements.first!) == "POSTED BY JOHN.DOE"
    }

    func testDoesNotRelayWrittenByErrors() {

        let sut = PostViewModel(post: post, commentsUseCases: successfulCommentsUseCases, usersUseCases: failingUsersUseCases, pipeline: ImagePipeline.shared)

        let writtenBy = sut.writtenBy.asObservable().take(1)

        guard case .completed(let elements) = writtenBy.toBlocking().materialize() else {
            return XCTFail()
        }

        expect(self.failingUsersUseCases.requestedUsers) == [user.id]
        expect(elements.count) == 1
        expect(elements.first!) == ""
    }
}
