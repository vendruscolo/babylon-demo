//
//  PostViewModelTypeTests.swift
//  Babylon-DemoTests
//
//  Created by Alessandro Vendruscolo on 08/07/2019.
//  Copyright © 2019 AVEdesign. All rights reserved.
//

@testable import Babylon_Demo
import Nimble
import Nuke
import XCTest

class PostViewModelTypeTests: XCTestCase {

    func testExcerpt() {

        let post = Post(
            id: 1,
            title: "The title",
            body: "A body that's long enough to be made of eleven words",
            authorId: 1,
            commentsId: []
        )
        let sut = PostViewModel(post: post, commentsUseCases: CommentsUseCasesWithErrorRecordingStub(error: TestError.network), usersUseCases: UsersUseCasesWithErrorRecordingStub(error: TestError.network), pipeline: ImagePipeline.shared)

        expect(sut.excerpt(readMore: "ciao")) == "A body that's long enough to be made of eleven\nciao"

    }
}
