//
//  PostDetailViewModelTests.swift
//  Babylon-DemoTests
//
//  Created by Alessandro Vendruscolo on 07/07/2019.
//  Copyright © 2019 AVEdesign. All rights reserved.
//

@testable import Babylon_Demo
import Nimble
import Nuke
import ReSwift
import RxSwift
import RxTest
import XCTest

class PostDetailViewModelTests: XCTestCase {

    let post = makePostWith(id: 1)
    let localComment = makeCommentWith(id: 1)
    let remoteComment = makeCommentWith(id: 2)
    let user = makeUserWith(id: 10)

    var testScheduler: TestScheduler!

    override func setUp() {
        super.setUp()
        testScheduler = TestScheduler(initialClock: 0)
    }

    func testPost() {
        let sut = PostDetailViewModel(post: post, state: .never(), pipeline: ImagePipeline.shared)
        expect(sut.post) == post
    }

    func testCommentsList() {

        let localCommentsLoaded = PostDetailState(author: nil, post: post, comments: [localComment])
        let remoteCommentsLoaded = PostDetailState(author: nil, post: post, comments: [remoteComment])

        let comments = testScheduler.createObserver([Comment].self)
        let states = testScheduler.createColdObservable([
            .next(10, localCommentsLoaded),
            .next(20, remoteCommentsLoaded),
        ])

        let sut = PostDetailViewModel(post: post, state: states.asObservable(), pipeline: ImagePipeline.shared)
        _ = sut.commentsList.drive(comments)

        testScheduler.start()

        expect(comments.events) == [
            .next(10, [localComment]),
            .next(20, [remoteComment])
        ]
    }

    func testWrittenBy() {

        let authorLoaded = PostDetailState(author: user, post: post, comments: [])

        let writtenBy = testScheduler.createObserver(String.self)
        let states = testScheduler.createColdObservable([
            .next(10, authorLoaded),
        ])

        let sut = PostDetailViewModel(post: post, state: states.asObservable(), pipeline: ImagePipeline.shared)
        _ = sut.writtenBy.drive(writtenBy)

        testScheduler.start()

        expect(writtenBy.events) == [
            .next(10, "POSTED BY JOHN DOE"),
        ]

    }

    func testDoesNotRelayWrittenByErrors() {

        let authorLoaded = PostDetailState(author: nil, post: post, comments: [])

        let writtenBy = testScheduler.createObserver(String.self)
        let states = testScheduler.createColdObservable([
            .next(10, authorLoaded),
        ])

        let sut = PostDetailViewModel(post: post, state: states.asObservable(), pipeline: ImagePipeline.shared)
        _ = sut.writtenBy.drive(writtenBy)

        testScheduler.start()

        expect(writtenBy.events) == [
            .next(10, ""),
        ]

    }
}
