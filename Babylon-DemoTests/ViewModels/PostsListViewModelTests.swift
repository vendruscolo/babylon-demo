//
//  PostsListViewModelTests.swift
//  Babylon-DemoTests
//
//  Created by Alessandro Vendruscolo on 07/07/2019.
//  Copyright © 2019 AVEdesign. All rights reserved.
//

@testable import Babylon_Demo
import Nimble
import Nuke
import ReSwift
import RxSwift
import RxTest
import XCTest

class PostsListViewModelTests: XCTestCase {

    let localPosts = [makePostWith(id: 1)]
    let remotePosts = [makePostWith(id: 2)]

    var testScheduler: TestScheduler!

    override func setUp() {
        super.setUp()
        testScheduler = TestScheduler(initialClock: 0)
    }

    func testIsLoading() {

        let idle = PostsState(remoteLoadStatus: .idle, posts: [], postsSource: .localCache)
        let loading = PostsState(remoteLoadStatus: .inProgress, posts: [], postsSource: .localCache)
        let completed = PostsState(remoteLoadStatus: .completed, posts: [], postsSource: .remote)
        let failed = PostsState(remoteLoadStatus: .failed(TestError.network), posts: [], postsSource: .localCache)

        let isLoading = testScheduler.createObserver(Bool.self)
        let states = testScheduler.createColdObservable([
            .next(10, idle),
            .next(20, loading),
            .next(30, completed),
            .next(40, failed)
        ])

        let sut = PostsListViewModel(state: states.asObservable())
        _ = sut.isLoading.drive(isLoading)

        testScheduler.start()

        expect(isLoading.events) == [
            .next(10, false),
            .next(20, true),
            .next(30, false),
            .next(40, false)
        ]

    }

    func testPostFetching() {

        let localFetched = PostsState(remoteLoadStatus: .inProgress, posts: localPosts, postsSource: .localCache)
        let remoteFetched = PostsState(remoteLoadStatus: .completed, posts: remotePosts, postsSource: .remote)

        let posts = testScheduler.createObserver([Post].self)
        let states = testScheduler.createColdObservable([
            .next(10, localFetched),
            .next(20, remoteFetched)
        ])

        let sut = PostsListViewModel(state: states.asObservable())
        _ = sut.posts.drive(posts)

        testScheduler.start()

        expect(posts.events) == [
            .next(10, localPosts),
            .next(20, remotePosts)
        ]
    }

    func testStatusMessage() {

        let firstFetch = PostsState(remoteLoadStatus: .inProgress, posts: [], postsSource: .localCache)
        let refresh = PostsState(remoteLoadStatus: .inProgress, posts: localPosts, postsSource: .localCache)
        let remoteFetched = PostsState(remoteLoadStatus: .completed, posts: remotePosts, postsSource: .remote)
        let firstFetchFailed = PostsState(remoteLoadStatus: .failed(TestError.network), posts: [], postsSource: .localCache)
        let refreshFailed = PostsState(remoteLoadStatus: .failed(TestError.network), posts: localPosts, postsSource: .localCache)

        let statusMessage = testScheduler.createObserver(PostsListStatusMessage.self)
        let states = testScheduler.createColdObservable([
            .next(10, firstFetch),
            .next(20, refresh),
            .next(30, remoteFetched),
            .next(40, firstFetchFailed),
            .next(50, refreshFailed)
        ])

        let sut = PostsListViewModel(state: states.asObservable())
        _ = sut.statusMessage.drive(statusMessage)

        testScheduler.start()

        expect(statusMessage.events) == [
            .next(10, PostsListStatusMessage.loading("Loading…")),
            .next(20, PostsListStatusMessage.loading("Refreshing posts…")),
            .next(30, PostsListStatusMessage.none),
            .next(40, PostsListStatusMessage.warning("An error occurred while loading posts.")),
            .next(50, PostsListStatusMessage.warning("An error occurred while refreshing posts. Showing cached data."))
        ]
    }

    func testDoesNotRelayErrors() {

        let isLoading = testScheduler.createObserver(Bool.self)
        let posts = testScheduler.createObserver([Post].self)
        let statusMessage = testScheduler.createObserver(PostsListStatusMessage.self)

        let states: TestableObservable<PostsState> = testScheduler.createHotObservable([
            .error(10, TestError.network)
        ])

        let sut = PostsListViewModel(state: states.asObservable())
        _ = sut.isLoading.drive(isLoading)
        _ = sut.posts.drive(posts)
        _ = sut.statusMessage.drive(statusMessage)

        testScheduler.start()

        // Here we're checking the first event because the observables complete
        expect(isLoading.events.first) == .next(10, false)
        expect(posts.events.first) == .next(10, [])
        expect(statusMessage.events.first) == .next(10, PostsListStatusMessage.none)
    }
}
