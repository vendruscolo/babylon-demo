//
//  CommentViewModelTests.swift
//  Babylon-DemoTests
//
//  Created by Alessandro Vendruscolo on 07/07/2019.
//  Copyright © 2019 AVEdesign. All rights reserved.
//

import Babylon_Demo
import Nimble
import XCTest

class CommentViewModelTests: XCTestCase {

    func testAuthorCommented() {
        let comment = makeCommentWith(id: 10)
        let sut = CommentViewModel(comment: comment)

        // TODO: Maybe we should take in consideration the locale of the app?
        expect(sut.authorCommented) == "THE AUTHOR COMMENTED:"
    }
}
