//
//  UsersUseCases.swift
//  Babylon-DemoTests
//
//  Created by Alessandro Vendruscolo on 07/07/2019.
//  Copyright © 2019 AVEdesign. All rights reserved.
//

import Babylon_Demo
import Nimble
import RxBlocking
import RxSwift
import XCTest

class UsersUseCasesTests: XCTestCase {

    let post = makePostWith(id: 10)
    let localUser = makeUserWith(id: 1)
    let remoteUser = makeUserPayload()

    var successfulDAO: UsersDAOSuccessfulStub!
    var partiallyFailingDAO: UsersDAOFailingGetSuccesfulStoreStub!
    var successfulRepository: UsersRepositorySuccessfulStub!
    var failingRepository: UsersRepositoryFailingStub!

    override func setUp() {
        super.setUp()
        successfulDAO = UsersDAOSuccessfulStub(user: self.localUser)
        partiallyFailingDAO = UsersDAOFailingGetSuccesfulStoreStub(error: TestError.network)
        successfulRepository = UsersRepositorySuccessfulStub(user: self.remoteUser)
        failingRepository = UsersRepositoryFailingStub(error: TestError.network)
    }

    func testDoesNotFetchRemoteIfLocalSucceeds() {

        let sut = UsersUseCases(usersDAO: successfulDAO, usersRepository: successfulRepository)

        guard case .completed(let elements) = sut.getUserProfile(with: post.authorId).toBlocking().materialize() else {
            return XCTFail()
        }

        expect(self.successfulDAO.requestedUsers) == [post.authorId]
        expect(self.successfulRepository.requestedUsers).to(beEmpty())
        expect(self.successfulDAO.persistedUsers).to(beEmpty())

        let (local, localDataSource) = elements.first!
        expect(local) == localUser
        expect(localDataSource) == .localCache

        expect(elements.count) == 1
    }

    func testFetchesRemote() {

        let sut = UsersUseCases(usersDAO: partiallyFailingDAO, usersRepository: successfulRepository)

        let result = sut.getUserProfile(with: post.authorId).debug().toBlocking().materialize()
        guard case .completed(let elements) = result else {
            return XCTFail()
        }

        expect(self.partiallyFailingDAO.requestedUsers) == [post.authorId]
        expect(self.successfulRepository.requestedUsers) == [post.authorId]
        expect(self.partiallyFailingDAO.persistedUsers).toNot(beEmpty())

        let (remote, remoteDataSource) = elements.first!
        expect(remote).notTo(beNil())
        expect(remoteDataSource) == .remote

        expect(elements.count) == 1

    }
}
