//
//  CommentsUseCasesTests.swift
//  Babylon-DemoTests
//
//  Created by Alessandro Vendruscolo on 07/07/2019.
//  Copyright © 2019 AVEdesign. All rights reserved.
//

import Babylon_Demo
import Nimble
import RxBlocking
import RxSwift
import XCTest

class CommentsUseCasesTests: XCTestCase {

    let post = makePostWith(id: 10)
    let localComments = [makeCommentWith(id: 1)]
    let remoteComments = [makeCommentPayload()]

    var successfulDAO: CommentsDAOSuccessfulStub!
    var successfulRepository: CommentsRepositorySuccessfulStub!
    var failingRepository: CommentsRepositoryFailingStub!

    override func setUp() {
        super.setUp()
        successfulDAO = CommentsDAOSuccessfulStub(comments: self.localComments)
        successfulRepository = CommentsRepositorySuccessfulStub(comments: self.remoteComments)
        failingRepository = CommentsRepositoryFailingStub(error: TestError.network)
    }

    func testFetchesLocalFirstAndThenPersistsRemote() {

        let sut = CommentsUseCases(
            commentsDAO: successfulDAO,
            commentsRepository: successfulRepository
        )

        guard case .completed(let elements) = sut.getCommentsOf(post: post).toBlocking().materialize() else {
            return XCTFail()
        }
        let (local, localDataSource) = elements.first!
        let (remote, remoteDataSource) = elements.last!

        expect(self.successfulRepository.requestedComments) == [post.id]
        expect(self.successfulDAO.persistedComments) == [remote]

        expect(local) == localComments
        expect(localDataSource) == .localCache

        expect(remote.count) == 1
        expect(remoteDataSource) == .remote
    }

    func testReportsRemoteErrorWithoutPersisting() {

        let sut = CommentsUseCases(
            commentsDAO: successfulDAO,
            commentsRepository: failingRepository
        )

        guard case .failed(let elements, let error) = sut.getCommentsOf(post: post).toBlocking().materialize() else {
            return XCTFail()
        }
        let (local, localDataSource) = elements.first!

        expect(self.failingRepository.requestedComments) == [post.id]
        expect(self.successfulDAO.persistedComments).to(beEmpty())

        expect(elements.count) == 1
        expect(error as? TestError) == TestError.network

        expect(local) == localComments
        expect(localDataSource) == .localCache
    }
}
