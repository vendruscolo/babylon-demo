//
//  PostsUseCasesTests.swift
//  Babylon-DemoTests
//
//  Created by Alessandro Vendruscolo on 07/07/2019.
//  Copyright © 2019 AVEdesign. All rights reserved.
//

import Babylon_Demo
import Nimble
import RxBlocking
import RxSwift
import XCTest

class PostsUseCasesTests: XCTestCase {

    let localPosts = [makePostWith(id: 3)]
    let remotePosts = [makePostPayload()]
    let localComments = [makeCommentWith(id: 1)]
    let remoteComments = [makeCommentPayload()]

    var successfulPostsDAO: PostsDAOSuccessfulStub!
    var successfulPostsRepository: PostsRepositorySuccessfulStub!
    var failingPostsRepository: PostsRepositoryFailingStub!
    var successfulCommentsDAO: CommentsDAOSuccessfulStub!
    var successfulCommentsRepository: CommentsRepositorySuccessfulStub!
    var failingCommentsRepository: CommentsRepositoryFailingStub!

    override func setUp() {
        super.setUp()
        successfulPostsDAO = PostsDAOSuccessfulStub(posts: self.localPosts)
        successfulPostsRepository = PostsRepositorySuccessfulStub(posts: self.remotePosts)
        failingPostsRepository = PostsRepositoryFailingStub(error: TestError.network)
        successfulCommentsDAO = CommentsDAOSuccessfulStub(comments: self.localComments)
        successfulCommentsRepository = CommentsRepositorySuccessfulStub(comments: self.remoteComments)
        failingCommentsRepository = CommentsRepositoryFailingStub(error: TestError.network)
    }

    func testFetchesLocalFirstThenPersistsRemote() {

        let sut = PostsUseCases(
            postsDAO: successfulPostsDAO,
            postsRepository: successfulPostsRepository,
            commentsDAO: successfulCommentsDAO,
            commentsRepository: successfulCommentsRepository
        )

        guard case .completed(let elements) = sut.getAllPosts().toBlocking().materialize() else {
            return XCTFail()
        }
        let (local, localDataSource) = elements.first!
        let (remote, remoteDataSource) = elements.last!

        expect(self.successfulPostsDAO.requests) == 1
        expect(self.successfulPostsRepository.requests) == 1
        expect(self.successfulCommentsRepository.requests) == 1
        expect(self.successfulPostsDAO.persistedPosts) == [remote]
        expect(self.successfulCommentsDAO.persistedComments).notTo(beEmpty())
        
        expect(local) == localPosts
        expect(localDataSource) == .localCache

        expect(remote.count) == 1
        expect(remoteDataSource) == .remote
    }

    func testReportsRemotePostsErrorWithoutPersisting() {

        let sut = PostsUseCases(
            postsDAO: successfulPostsDAO,
            postsRepository: failingPostsRepository,
            commentsDAO: successfulCommentsDAO,
            commentsRepository: successfulCommentsRepository
        )

        guard case .failed(let elements, let error) = sut.getAllPosts().toBlocking().materialize() else {
            return XCTFail()
        }
        let (local, localDataSource) = elements.first!

        expect(self.successfulPostsDAO.requests) == 1
        expect(self.failingPostsRepository.requests) == 1
        expect(self.successfulCommentsRepository.requests) == 1
        expect(self.successfulPostsDAO.persistedPosts).to(beEmpty())
        expect(self.successfulCommentsDAO.persistedComments).to(beEmpty())

        expect(elements.count) == 1
        expect(error as? TestError) == TestError.network

        expect(local) == localPosts
        expect(localDataSource) == .localCache
    }

    func testReportsRemoteCommentsErrorWithoutPersisting() {

        let sut = PostsUseCases(
            postsDAO: successfulPostsDAO,
            postsRepository: successfulPostsRepository,
            commentsDAO: successfulCommentsDAO,
            commentsRepository: failingCommentsRepository
        )

        guard case .failed(let elements, let error) = sut.getAllPosts().toBlocking().materialize() else {
            return XCTFail()
        }
        let (local, localDataSource) = elements.first!

        expect(self.successfulPostsDAO.requests) == 1
        expect(self.successfulPostsRepository.requests) == 1
        expect(self.failingCommentsRepository.requests) == 1
        expect(self.successfulPostsDAO.persistedPosts).to(beEmpty())
        expect(self.successfulCommentsDAO.persistedComments).to(beEmpty())

        expect(elements.count) == 1
        expect(error as? TestError) == TestError.network

        expect(local) == localPosts
        expect(localDataSource) == .localCache
    }
}
