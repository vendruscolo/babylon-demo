//
//  PostDetailReducerTests.swift
//  Babylon-DemoTests
//
//  Created by Alessandro Vendruscolo on 06/07/2019.
//  Copyright © 2019 AVEdesign. All rights reserved.
//

@testable import Babylon_Demo
import Nimble
import XCTest

class PostDetailReducerTests: XCTestCase {

    func testSetPostDetail() {
        let post = makePostWith(id: 1)
        let action = SetPostDetail(data: post)

        guard let sut = postDetailReducer(action: action, state: nil) else {
            return XCTFail()
        }

        expect(sut.author).to(beNil())
        expect(sut.post) == post
        expect(sut.comments).to(beEmpty())
    }

    func testSetPostDetailClearsPreviousData() {
        let initialPost = makePostWith(id: 1)
        let initialAuthor = makeUserWith(id: 1)
        let initialComments = makeComments()

        let initial = PostDetailState(author: initialAuthor, post: initialPost, comments: initialComments)
        let post = makePostWith(id: 2)
        let action = SetPostDetail(data: post)

        guard let sut = postDetailReducer(action: action, state: initial) else {
            return XCTFail()
        }

        expect(sut.author).to(beNil())
        expect(sut.post) == post
        expect(sut.comments).to(beEmpty())
    }

    func testSetAuthor() {
        let post = makePostWith(id: 1)
        let author = makeUserWith(id: 1)
        let action = SetAuthorForCurrentPost(data: author)
        let initial = PostDetailState(author: nil, post: post, comments: [])

        guard let sut = postDetailReducer(action: action, state: initial) else {
            return XCTFail()
        }

        expect(sut.author) == author
        expect(sut.post) == post
        expect(sut.comments).to(beEmpty())
    }

    func testSetComments() {
        let post = makePostWith(id: 1)
        let comments = makeComments()
        let action = SetCommentsForCurrentPost(data: comments)
        let initial = PostDetailState(author: nil, post: post, comments: [])

        guard let sut = postDetailReducer(action: action, state: initial) else {
            return XCTFail()
        }

        expect(sut.author).to(beNil())
        expect(sut.post) == post
        expect(sut.comments) == comments
    }

    func testSetNoPostDetail() {
        let author = makeUserWith(id: 1)
        let post = makePostWith(id: 1)
        let comments = makeComments()
        let action = SetNoPostDetail()
        let initial = PostDetailState(author: author, post: post, comments: comments)

        let sut = postDetailReducer(action: action, state: initial)

        expect(sut).to(beNil())
    }
}
