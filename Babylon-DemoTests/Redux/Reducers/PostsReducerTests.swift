//
//  PostsReducerTests.swift
//  Babylon-DemoTests
//
//  Created by Alessandro Vendruscolo on 06/07/2019.
//  Copyright © 2019 AVEdesign. All rights reserved.
//

@testable import Babylon_Demo
import Nimble
import XCTest

class PostsReducerTests: XCTestCase {

    func testSetLoading() {
        let action = SetPostsAreLoading()

        let sut = postsReducer(action: action, state: nil)

        expect(sut.remoteLoadStatus) == .inProgress
        expect(sut.posts).to(beEmpty())
        expect(sut.postsSource) == .localCache
    }

    func testSetLocalPosts() {
        let loadedPosts = [makePostWith(id: 1)]
        let action = SetLocalPosts(data: loadedPosts)
        let initial = PostsState(
            remoteLoadStatus: .idle,
            posts: [],
            postsSource: .localCache
        )

        let sut = postsReducer(action: action, state: initial)

        expect(sut.remoteLoadStatus) == .idle
        expect(sut.posts) == loadedPosts
        expect(sut.postsSource) == .localCache
    }

    func testSetRemotePosts() {
        let loadedPosts = [makePostWith(id: 2)]
        let action = SetRemotePostsCompleted(data: loadedPosts)
        let initialPosts = [makePostWith(id: 1)]
        let initial = PostsState(
            remoteLoadStatus: .inProgress,
            posts: initialPosts,
            postsSource: .localCache
        )

        let sut = postsReducer(action: action, state: initial)

        expect(sut.remoteLoadStatus) == .completed
        expect(sut.posts) == loadedPosts
        expect(sut.postsSource) == .remote
    }

    func testRemotePostsFailed() {
        let action = SetRemotePostsLoadFailed(error: TestError.network)
        let initialPosts = [makePostWith(id: 1)]
        let initial = PostsState(
            remoteLoadStatus: .inProgress,
            posts: initialPosts,
            postsSource: .localCache
        )

        let sut = postsReducer(action: action, state: initial)

        guard
            case .failed(let failure) = sut.remoteLoadStatus,
            let error = failure as? TestError
        else {
            return XCTFail()
        }

        expect(error) == TestError.network
        expect(sut.posts) == initialPosts
        expect(sut.postsSource) == .localCache
    }
}
