//
//  UsersThunkTests.swift
//  Babylon-DemoTests
//
//  Created by Alessandro Vendruscolo on 07/07/2019.
//  Copyright © 2019 AVEdesign. All rights reserved.
//

@testable import Babylon_Demo
import Nimble
import ReSwift
import ReSwiftThunk
import RxSwift
import XCTest

class UsersThunkTests: XCTestCase {

    let post = makePostWith(id: 1)
    let user = makeUserWith(id: 1)

    var disposeBag: DisposeBag!

    override func setUp() {
        super.setUp()
        disposeBag = DisposeBag()
    }

    func testDoesNotFetchIfNoPost() {

        let useCase = UsersUseCasesSuccesfulRecordingStub(
            user: user,
            dataSource: .localCache
        )
        let sut = fetchAuthorThunk(useCases: useCase, disposeBag: disposeBag)

        let initial = AppState()

        let store = ExpectThunk(sut)
            .getsState(initial)
            .run()

        expect(store.dispatched).to(beEmpty())
        expect(useCase.requestedUsers).to(beEmpty())
    }

    func testFetchesAuthor() {

        let useCase = UsersUseCasesSuccesfulRecordingStub(
            user: user,
            dataSource: .localCache
        )
        let sut = fetchAuthorThunk(useCases: useCase, disposeBag: disposeBag)

        let initialPostDetailState = PostDetailState(author: nil, post: post, comments: [])
        let initial = AppState(postsState: PostsState(), postDetailState: initialPostDetailState)

        ExpectThunk(sut)
            .getsState(initial)
            .dispatches(SetAuthorForCurrentPost(data: user))
            .run()

        expect(useCase.requestedUsers) == [post.authorId]
    }

    func testIgnoresError() {

        let useCase = UsersUseCasesWithErrorRecordingStub(
            error: TestError.network
        )
        let sut = fetchAuthorThunk(useCases: useCase, disposeBag: disposeBag)

        let initialPostDetailState = PostDetailState(author: nil, post: post, comments: [])
        let initial = AppState(postsState: PostsState(), postDetailState: initialPostDetailState)

        let store = ExpectThunk(sut)
            .getsState(initial)
            .run()

        expect(store.dispatched).to(beEmpty())
        expect(useCase.requestedUsers) == [post.authorId]
    }

}

