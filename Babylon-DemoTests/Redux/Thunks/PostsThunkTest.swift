//
//  PostsThunkTest.swift
//  Babylon-DemoTests
//
//  Created by Alessandro Vendruscolo on 07/07/2019.
//  Copyright © 2019 AVEdesign. All rights reserved.
//

@testable import Babylon_Demo
import Nimble
import ReSwift
import ReSwiftThunk
import RxSwift
import XCTest

class PostsThunkTests: XCTestCase {

    let stubbedPosts = makePosts()
    
    var useCase: PostsUseCasesSuccesfulRecordingStub!
    var disposeBag: DisposeBag!
    var sut: Thunk<AppState>!

    override func setUp() {
        super.setUp()
        useCase = PostsUseCasesSuccesfulRecordingStub(
            posts: [stubbedPosts, stubbedPosts],
            dataSources: [.localCache, .remote]
        )
        disposeBag = DisposeBag()
        sut = fetchPostsThunk(useCases: useCase, disposeBag: disposeBag)
    }

    func testStartFetchingPosts() {

        let initial = AppState()

        ExpectThunk(sut)
            .getsState(initial)
            .dispatches(SetPostsAreLoading())
            .dispatches(SetLocalPosts(data: stubbedPosts))
            .dispatches(SetRemotePostsCompleted(data: stubbedPosts))
            .run()

        expect(self.useCase.requests) == 1

    }

    func testDoesNotPerformTheRequestTwice() {

        let initialPostsState = PostsState(remoteLoadStatus: .inProgress, posts: [], postsSource: .localCache)
        let initial = AppState(postsState: initialPostsState, postDetailState: nil)

        let store = ExpectThunk(sut)
            .getsState(initial)
            .run()

        expect(self.useCase.requests) == 0
        expect(store.dispatched).to(beEmpty())

    }

    func testReportsRemoteError() {

        let failingUseCase = PostsUseCasesWithErrorRecordingStub(post: stubbedPosts, dataSource: .localCache, error: TestError.network)
        sut = fetchPostsThunk(useCases: failingUseCase, disposeBag: disposeBag)
        let initial = AppState()

        ExpectThunk(sut)
            .getsState(initial)
            .dispatches(SetPostsAreLoading())
            .dispatches(SetLocalPosts(data: stubbedPosts))
            .dispatches { action in
                guard
                    let action = action as? SetRemotePostsLoadFailed,
                    let error = action.error as? TestError
                else {
                    return XCTFail()
                }
                expect(error) == TestError.network
            }
            .run()

        expect(failingUseCase.requests) == 1

    }
}
