//
//  CommentsThunkTests.swift
//  Babylon-DemoTests
//
//  Created by Alessandro Vendruscolo on 06/07/2019.
//  Copyright © 2019 AVEdesign. All rights reserved.
//

@testable import Babylon_Demo
import Nimble
import ReSwift
import ReSwiftThunk
import RxSwift
import XCTest

class CommentsThunkTests: XCTestCase {

    let stubbedPost = makePostWith(id: 2)
    let stubbedComments = makeComments()
    
    var useCase: CommentsUseCasesSuccesfulRecordingStub!
    var disposeBag: DisposeBag!
    var sut: Thunk<AppState>!

    override func setUp() {
        super.setUp()
        useCase = CommentsUseCasesSuccesfulRecordingStub(
            comments: [stubbedComments, stubbedComments],
            dataSources: [.localCache, .remote]
        )
        disposeBag = DisposeBag()
        sut = fetchCommentsThunk(useCases: useCase, disposeBag: disposeBag)
    }

    func testDoesNotFetchCommentsIfNoPost() {

        let initial = AppState()
        ExpectThunk(sut)
            .getsState(initial)
            .run()

        expect(initial.postDetailState?.post).to(beNil())
        expect(self.useCase.requestedPosts).to(beEmpty())
    }

    func testDoesFetchCommentsForPost() {

        let initialPostDetailState = PostDetailState(author: nil, post: stubbedPost, comments: [])
        let initial = AppState(postsState: PostsState(), postDetailState: initialPostDetailState)

        ExpectThunk(sut)
            .getsState(initial)
            .dispatches(SetCommentsForCurrentPost(data: stubbedComments))
            .dispatches(SetCommentsForCurrentPost(data: stubbedComments))
            .run()

        expect(self.useCase.requestedPosts) == [stubbedPost]

    }

    func testIgnoresError() {

        let failingUseCase = PartiallyFailingCommentsUseCasesWithErrorRecordingStuff(comments: stubbedComments, dataSource: .localCache, error: TestError.network)
        sut = fetchCommentsThunk(useCases: failingUseCase, disposeBag: disposeBag)

        let initialPostDetailState = PostDetailState(author: nil, post: stubbedPost, comments: [])
        let initial = AppState(postsState: PostsState(), postDetailState: initialPostDetailState)

        let store = ExpectThunk(sut)
            .getsState(initial)
            .dispatches(SetCommentsForCurrentPost(data: stubbedComments))
            .run()

        expect(store.dispatched.count) == 1
        expect(failingUseCase.requestedPosts) == [stubbedPost]
    }
}
