//
//  NetworkMappersTests.swift
//  Babylon-DemoTests
//
//  Created by Alessandro Vendruscolo on 06/07/2019.
//  Copyright © 2019 AVEdesign. All rights reserved.
//

import Babylon_Demo
import Nimble
import XCTest

class NetworkMappersTests: XCTestCase {

    func testMapComment() {
        let payload = makeCommentPayload()
        let result = Comment(
            id: 23,
            postId: 4,
            author: "Sophia@arianna.co.uk",
            title: "quis tempora quidem nihil iste",
            body: "voluptates provident repellendus iusto perspiciatis ex fugiat"
        )

        expect(NetworkMappers.map(comment: payload)) == result

    }

    func testMapPostWithMatchingIds() {
        let postPayload = makePostPayload()
        let commentsPayload = makeCommentsPayload()
        let result = Post(
            id: 1,
            title: "sunt aut facere repellat provident occaecati excepturi optio reprehenderit",
            body: "suscipit recusandae consequuntur expedita et cum",
            authorId: 1,
            commentsId: [23]
        )

        expect(NetworkMappers.map(post: postPayload, comments: commentsPayload)) == result

    }

    func testMapPostWithoutMatchingIds() {
        let postPayload = makePostPayload(id: 6)
        let commentsPayload = makeCommentsPayload()
        let result = Post(
            id: 6,
            title: "sunt aut facere repellat provident occaecati excepturi optio reprehenderit",
            body: "suscipit recusandae consequuntur expedita et cum",
            authorId: 1,
            commentsId: []
        )

        expect(NetworkMappers.map(post: postPayload, comments: commentsPayload)) == result

    }

    func testMapUser() {
        let payload = makeUserPayload()
        let result = User(
            id: 1,
            name: "Leanne Graham",
            username: "Bret"
        )

        expect(NetworkMappers.map(user: payload)) == result

    }
}
