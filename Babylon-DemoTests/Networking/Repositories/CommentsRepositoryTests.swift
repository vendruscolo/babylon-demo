//
//  CommentsRepositoryTests.swift
//  Babylon-DemoTests
//
//  Created by Alessandro Vendruscolo on 06/07/2019.
//  Copyright © 2019 AVEdesign. All rights reserved.
//

import Babylon_Demo
import Moya
import Nimble
import RxBlocking
import XCTest

class CommentsRepositoryTests: XCTestCase {

    var provider: RecordingMoyaProvider<JSONPlaceholderTarget>!
    var sut: CommentsRepository!

    override func setUp() {
        super.setUp()
        provider = RecordingMoyaProvider.makeRecorder()
        sut = CommentsRepository(provider: provider)
    }

    func testGetAllComments() {
        let results = sut.getAllComments().toBlocking().materialize()
        guard case .completed(let commentElements) = results else {
            return XCTFail()
        }

        expect(self.provider.requestedTargets) == [.comments]
        expect(commentElements.count) == 1
        expect(commentElements.first).notTo(beEmpty())
    }

    func testGetCommentsOfPost() {
        let results = sut.getCommentsOf(post: 1).toBlocking().materialize()
        guard case .completed(let commentElements) = results else {
            return XCTFail()
        }

        expect(self.provider.requestedTargets) == [.commentsOf(Identifier<Post>(rawValue: 1))]
        expect(commentElements.count) == 1
        expect(commentElements.first).notTo(beEmpty())
    }
}
