//
//  UsersRepositoryTests.swift
//  Babylon-DemoTests
//
//  Created by Alessandro Vendruscolo on 06/07/2019.
//  Copyright © 2019 AVEdesign. All rights reserved.
//

import Babylon_Demo
import Moya
import Nimble
import RxBlocking
import XCTest

class UsersRepositoryTests: XCTestCase {

    var provider: RecordingMoyaProvider<JSONPlaceholderTarget>!
    var sut: UsersRepository!

    override func setUp() {
        super.setUp()
        provider = RecordingMoyaProvider.makeRecorder()
        sut = UsersRepository(provider: provider)
    }

    func testGetUser() {
        let results = sut.getUser(with: 1).toBlocking().materialize()
        guard case .completed(let userElements) = results else {
            return XCTFail()
        }

        expect(self.provider.requestedTargets) == [.user(Identifier<User>(rawValue: 1))]
        expect(userElements.count) == 1
        expect(userElements.first).notTo(beNil())
    }

}
