//
//  PostsRepositoryTests.swift
//  Babylon-DemoTests
//
//  Created by Alessandro Vendruscolo on 06/07/2019.
//  Copyright © 2019 AVEdesign. All rights reserved.
//

import Babylon_Demo
import Moya
import Nimble
import RxBlocking
import XCTest

class PostsRepositoryTests: XCTestCase {

    var provider: RecordingMoyaProvider<JSONPlaceholderTarget>!
    var sut: PostsRepository!

    override func setUp() {
        super.setUp()
        provider = RecordingMoyaProvider.makeRecorder()
        sut = PostsRepository(provider: provider)
    }

    func testGetAllPosts() {
        let results = sut.getAllPosts().toBlocking().materialize()
        guard case .completed(let postElements) = results else {
            return XCTFail()
        }

        expect(self.provider.requestedTargets) == [.posts]
        expect(postElements.count) == 1
        expect(postElements.first).notTo(beEmpty())
    }

}
